package com.iveso.doctorum.stub.schedule;

import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.stub.visit.VisitStub;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class ScheduleStub {
    private final Schedule schedule;

    private ScheduleStub() {
        this.schedule = new Schedule();
    }

    public static ScheduleStub newInstance() {
        return new ScheduleStub();
    }

    public ScheduleStub withId(Integer id) {
        schedule.setId(id);
        return this;
    }

    public ScheduleStub withDate(LocalDate date) {
        schedule.setDate(date);
        return this;
    }

    public ScheduleStub withDoctors(Integer... doctorIds) {
        List<Visit> visits = Arrays.stream(doctorIds)
                .map(Doctor::new)
                .map(Visit::new)
                .collect(Collectors.toList());

        schedule.setVisits(visits);
        return this;
    }

    public ScheduleStub addVisit(UnaryOperator<VisitStub> stub) {
        if(schedule.getVisits() == null) schedule.setVisits(new ArrayList<>());
        schedule.getVisits().add(stub.apply(VisitStub.newInstance()).build());
        return this;
    }

    public Schedule build() {
        return schedule;
    }
}

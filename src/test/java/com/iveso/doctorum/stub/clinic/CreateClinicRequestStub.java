package com.iveso.doctorum.stub.clinic;

import com.iveso.doctorum.controller.clinic.request.CreateClinic;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.UnaryOperator;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CreateClinicRequestStub {
    private String district;
    private String location;
    private String publicPlace;
    private String email;
    private Long phone;
    private Integer zipcode;
    private CreateClinic.County county;
    private Double latitude;
    private Double longitude;

    public static CreateClinicRequestStub newInstance() {
        return new CreateClinicRequestStub();
    }

    public CreateClinicRequestStub withDistrict(String district) {
        this.district = district;
        return this;
    }

    public CreateClinicRequestStub withLocation(String location) {
        this.location = location;
        return this;
    }

    public CreateClinicRequestStub withPublicPlace(String publicPlace) {
        this.publicPlace = publicPlace;
        return this;
    }

    public CreateClinicRequestStub withEmail(String email) {
        this.email = email;
        return this;
    }

    public CreateClinicRequestStub withPhone(Long phone) {
        this.phone = phone;
        return this;
    }

    public CreateClinicRequestStub withZipcode(Integer zipcode) {
        this.zipcode = zipcode;
        return this;
    }

    public CreateClinicRequestStub withCounty(UnaryOperator<CountyBuilder> countyBuilder) {
        this.county = countyBuilder.apply(new CountyBuilder()).build();
        return this;
    }

    public CreateClinicRequestStub withLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public CreateClinicRequestStub withLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public CreateClinic build() {
        return new CreateClinic(
                null,
                district,
                location,
                publicPlace,
                email,
                phone,
                latitude,
                longitude,
                zipcode,
                county
        );
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class CountyBuilder {
        private Integer id;
        private String name;
        private CreateClinic.State state;

        public CountyBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public CountyBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public CountyBuilder withState(UnaryOperator<StateBuilder> builder) {
            this.state = builder.apply(new StateBuilder()).build();
            return this;
        }

        public CreateClinic.County build() {
            return new CreateClinic.County(id, name, state);
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class StateBuilder {
        private Integer id;
        private String name;
        private String uf;

        public StateBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public StateBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public StateBuilder withUf(String uf) {
            this.uf = uf;
            return this;
        }

        public CreateClinic.State build() {
            return new CreateClinic.State(id, name, uf);
        }
    }
}

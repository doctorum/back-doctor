package com.iveso.doctorum.stub.clinic;

import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Contact;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import com.iveso.doctorum.stub.doctor.DoctorStub;

import java.util.ArrayList;
import java.util.function.UnaryOperator;

public class ClinicStub {
    private final Clinic clinic;
    private ClinicStub() {
        clinic = new Clinic();
    }

    public static ClinicStub newInstance() {
        return new ClinicStub();
    }

    public ClinicStub withId(Integer id) {
        clinic.setId(id);
        return this;
    }

    public ClinicStub withLocation(String location) {
        clinic.setLocation(location);
        return this;
    }

    public ClinicStub withPublicPlace(String publicPlace) {
        clinic.setPublicPlace(publicPlace);
        return this;
    }

    public ClinicStub withDistrict(String district) {
        clinic.setDistrict(district);
        return this;
    }

    public ClinicStub withContact(UnaryOperator<ContactBuilder> builder) {
        clinic.setContact(builder.apply(new ContactBuilder()).build());
        return this;
    }

    public ClinicStub withEmail(String email) {
        clinic.setEmail(email);
        return this;
    }

    public ClinicStub withZipcode(int zipcode) {
        clinic.setZipcode(zipcode);
        return this;
    }

    public ClinicStub withCounty(UnaryOperator<CountyBuilder> builder) {
        clinic.setCounty(builder.apply(new CountyBuilder()).build());
        return this;
    }

    public ClinicStub withLatitude(double latitude) {
        clinic.setLatitude(latitude);
        return this;
    }

    public ClinicStub withLongitude(double longitude) {
        clinic.setLongitude(longitude);
        return this;
    }

    public ClinicStub addDoctor(UnaryOperator<DoctorStub> builder) {
        if(clinic.getDoctors() == null) {
            clinic.setDoctors(new ArrayList<>());
        }

        clinic.getDoctors().add(builder.apply(DoctorStub.newInstance()).build());
        return this;
    }

    public Clinic build() {
        return clinic;
    }

    public static class ContactBuilder {
        private final Contact contact;

        private ContactBuilder() {
            contact = new Contact();
        }

        public ContactBuilder withContact(long number) {
            contact.setNumber(number);
            return this;
        }

        private Contact build() {
            return contact;
        }
    }

    public static class CountyBuilder {
        private final County county;

        private CountyBuilder() {
            county = new County();
        }

        public CountyBuilder withId(Integer id) {
            county.setId(id);
            return this;
        }

        public CountyBuilder withName(String name) {
            county.setName(name);
            return this;
        }

        public CountyBuilder withState(UnaryOperator<StateBuilder> builder) {
            county.setState(builder.apply(new StateBuilder()).build());
            return this;
        }

        public County build() {
            return county;
        }
    }

    public static class StateBuilder {
        private final State state;

        private StateBuilder() {
            state = new State();
        }

        public StateBuilder withUf(String uf) {
            state.setUf(uf);
            return this;
        }

        public State build() {
            return state;
        }
    }
}

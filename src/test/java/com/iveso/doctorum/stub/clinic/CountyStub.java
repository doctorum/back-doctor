package com.iveso.doctorum.stub.clinic;

import com.iveso.doctorum.repository.entity.County;

public class CountyStub {
    private final County county;

    private CountyStub() {
        county = new County();
    }

    public static CountyStub newInstance() {
        return new CountyStub();
    }

    public CountyStub withId(int id) {
        county.setId(id);
        return this;
    }

    public CountyStub withName(String name) {
        county.setName(name);
        return this;
    }

    public County build() {
        return county;
    }
}

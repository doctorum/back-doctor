package com.iveso.doctorum.stub.doctor;

import com.iveso.doctorum.repository.entity.Contact;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Email;
import com.iveso.doctorum.stub.clinic.ClinicStub;

import java.util.ArrayList;
import java.util.function.UnaryOperator;

public class DoctorStub {
    private final Doctor doctor;

    private DoctorStub() {
        doctor = new Doctor();
    }

    public static DoctorStub newInstance() {
        return new DoctorStub();
    }

    public DoctorStub withId(Integer id) {
        doctor.setId(id);
        return this;
    }

    public DoctorStub withName(String name) {
        doctor.setName(name);
        return this;
    }

    public DoctorStub withNickname(String nickname) {
        doctor.setNickname(nickname);
        return this;
    }

    public DoctorStub withExpertise(String expertise) {
        doctor.setExpertise(expertise);
        return this;
    }

    public DoctorStub withBirth(String birth) {
        doctor.setBirth(birth);
        return this;
    }

    public DoctorStub withCrm(String crm) {
        doctor.setCrm(crm);
        return this;
    }

    public DoctorStub withSector(String sector) {
        doctor.setSector(sector);
        return this;
    }

    public DoctorStub withGraduationYear(Integer graduationYear) {
        doctor.setGraduationYear(graduationYear);
        return this;
    }

    public DoctorStub withContact(UnaryOperator<ContactStub> stub) {
        doctor.setContact(stub.apply(new ContactStub()).build());
        return this;
    }

    public DoctorStub withEmail(UnaryOperator<EmailStub> stub) {
        doctor.setEmail(stub.apply(new EmailStub()).build());
        return this;
    }

    public DoctorStub addClinic(UnaryOperator<ClinicStub> stub) {
        if (doctor.getClinics() == null) doctor.setClinics(new ArrayList<>());
        doctor.getClinics().add(stub.apply(ClinicStub.newInstance()).build());
        return this;
    }

    public Doctor build() {
        return doctor;
    }

    public class ContactStub {
        private final Contact contact;

        private ContactStub() {
            contact = new Contact();
        }

        public ContactStub withContact(Long number) {
            contact.setNumber(number);
            return this;
        }

        private Contact build() {
            return contact;
        }
    }

    public class EmailStub {
        private final Email email;

        private EmailStub() {
            email = new Email();
        }

        public EmailStub withAddress(String address) {
            email.setAddress(address);
            return this;
        }

        private Email build() {
            return email;
        }
    }
}

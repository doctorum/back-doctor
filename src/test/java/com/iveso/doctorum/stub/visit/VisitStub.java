package com.iveso.doctorum.stub.visit;

import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.stub.doctor.DoctorStub;

import java.util.function.UnaryOperator;

public class VisitStub {
    private Visit visit;

    private VisitStub() {
        visit = new Visit();
    }

    public static VisitStub newInstance() {
        return new VisitStub();
    }

    public VisitStub withId(Integer id) {
        visit.setId(id);
        return this;
    }

    public VisitStub withDoctor(UnaryOperator<DoctorStub> stub) {
        visit.setDoctor(stub.apply(DoctorStub.newInstance()).build());
        return this;
    }

    public VisitStub withObservation(String observation) {
        visit.setObservation(observation);
        return this;
    }

    public VisitStub isVisited(boolean isVisited) {
        visit.setVisited(isVisited);
        return this;
    }

    public Visit build() {
        return visit;
    }
}

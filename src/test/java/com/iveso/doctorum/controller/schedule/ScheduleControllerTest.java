package com.iveso.doctorum.controller.schedule;

import com.iveso.doctorum.config.IntegrationTestBasics;
import com.iveso.doctorum.repository.ClinicRepository;
import com.iveso.doctorum.repository.CountyRepository;
import com.iveso.doctorum.repository.DoctorRepository;
import com.iveso.doctorum.repository.ScheduleRepository;
import com.iveso.doctorum.stub.clinic.ClinicStub;
import com.iveso.doctorum.stub.clinic.CountyStub;
import com.iveso.doctorum.stub.doctor.DoctorStub;
import com.iveso.doctorum.stub.schedule.ScheduleStub;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.mockito.Mockito.when;

@Disabled
@SuppressWarnings("unused")
class ScheduleControllerTest extends IntegrationTestBasics {

    @Mock
    private Clock clock;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private CountyRepository countyRepository;

    private static final ZonedDateTime NOW = ZonedDateTime.of(
            2024, 1, 3,
            16, 4, 23, 0,
            ZoneId.of("GMT")
    );

    @BeforeEach
    void setup() {
        when(clock.getZone()).thenReturn(NOW.getZone());
        when(clock.instant()).thenReturn(NOW.toInstant());
    }

    @Test
    void shouldCreateSchedule() throws Exception {
        doctorRepository.saveAll(List.of(
                DoctorStub.newInstance().withNickname("João").build(),
                DoctorStub.newInstance().withNickname("Matheus").build(),
                DoctorStub.newInstance().withNickname("Marina").build(),
                DoctorStub.newInstance().withNickname("Felipe").build(),
                DoctorStub.newInstance().withNickname("Pedro").build()
        ));

        mvc.perform(MockMvcRequestBuilders
                        .post("/schedule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "date": "2024-01-03",
                                    "doctors": [1, 2, 3, 5]
                                }
                                """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isCreated(),
                        MockMvcResultMatchers.header().string("Location", Matchers.is("/schedule/1"))
                );
    }

    @Test
    void shouldUpdateSchedule() throws Exception {
        doctorRepository.saveAll(List.of(
                DoctorStub.newInstance().withNickname("João").build(),
                DoctorStub.newInstance().withNickname("Matheus").build(),
                DoctorStub.newInstance().withNickname("Marina").build(),
                DoctorStub.newInstance().withNickname("Felipe").build(),
                DoctorStub.newInstance().withNickname("Pedro").build()
        ));

        scheduleRepository.save(ScheduleStub.newInstance()
                .withDate(LocalDate.now(clock))
                .withDoctors(1, 2, 3, 5)
                .build());

        mvc.perform(MockMvcRequestBuilders
                        .patch("/schedule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id": 1,
                                    "doctors": [1, 2, 4, 5]
                                }
                                """))
                .andExpect(
                        MockMvcResultMatchers.status().isNoContent()
                );
    }

    @Test
    void shouldThrowNotFoundWhenUpdateSchedule() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .patch("/schedule")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id": 1,
                                    "doctors": [1, 2, 3]
                                }
                                """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isNotFound(),
                        MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Schedule with id 1 not found"))
                );
    }

    @Test
    void shouldListScheduledDaysOfMonth() throws Exception {
        LocalDate date = LocalDate.now(clock);

        scheduleRepository.saveAll(List.of(
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(10)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(18)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(21)).build(),
                ScheduleStub.newInstance().withDate(date.withMonth(2).withDayOfMonth(21)).build()
        ));
        mvc.perform(MockMvcRequestBuilders
                .get("/schedule/days")
                .param("month", "1")
                .param("year", "2024"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.size()", Matchers.is(3)),
                        MockMvcResultMatchers.jsonPath("$[0]", Matchers.is(10)),
                        MockMvcResultMatchers.jsonPath("$[1]", Matchers.is(18)),
                        MockMvcResultMatchers.jsonPath("$[2]", Matchers.is(21))
                );
    }

    @Test
    void shouldFindScheduleByFilters() throws Exception {
        final int ID = 1;
        countyRepository.save(CountyStub.newInstance()
                .withId(ID)
                .withName("Estreito")
                .build());

        clinicRepository.save(ClinicStub.newInstance()
                .withCounty(county -> county.withId(ID))
                .withDistrict("Centro")
                .withLatitude(-15.7351936)
                .withLongitude(-48.2869248)
                .build());

        doctorRepository.save(DoctorStub.newInstance()
                .withNickname("Jão")
                .withName("João")
                .withExpertise("PEDIATRA")
                .addClinic(clinic -> clinic.withId(ID))
                .build());

        scheduleRepository.save(ScheduleStub.newInstance()
                .withDate(LocalDate.now(clock))
                .addVisit(visit -> visit
                        .isVisited(true)
                        .withDoctor(doctor -> doctor.withId(ID)))
                .build());

        mvc.perform(MockMvcRequestBuilders
                .get("/schedule")
                .param("date", LocalDate.now(clock).toString()))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.visits.size()", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.visits[0].id", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.visits[0].nickname", Matchers.is("Jão")),
                        MockMvcResultMatchers.jsonPath("$.visits[0].name", Matchers.is("João")),
                        MockMvcResultMatchers.jsonPath("$.visits[0].county", Matchers.is("Estreito")),
                        MockMvcResultMatchers.jsonPath("$.visits[0].district", Matchers.is("Centro")),
                        MockMvcResultMatchers.jsonPath("$.visits[0].expertise", Matchers.is("PEDIATRA")),
                        MockMvcResultMatchers.jsonPath("$.visits[0].latitude", Matchers.is(-15.7351936)),
                        MockMvcResultMatchers.jsonPath("$.visits[0].longitude", Matchers.is(-48.2869248)),
                        MockMvcResultMatchers.jsonPath("$.visits[0].visited", Matchers.is(true))
                );
    }

    @Test
    void shouldFindScheduleById() throws Exception {
        final int ID = 1;
        countyRepository.save(CountyStub.newInstance()
                .withId(ID)
                .withName("Estreito")
                .build());

        clinicRepository.save(ClinicStub.newInstance()
                .withCounty(county -> county.withId(ID))
                .withDistrict("Centro")
                .build());

        doctorRepository.saveAll(List.of(
                DoctorStub.newInstance()
                        .withNickname("Jão")
                        .withName("João")
                        .withExpertise("PEDIATRA")
                        .addClinic(clinic -> clinic.withId(ID))
                        .build(),

                DoctorStub.newInstance()
                        .withNickname("Mary")
                        .withName("Maria")
                        .withExpertise("CLINICO GERAL")
                        .addClinic(clinic -> clinic.withId(ID))
                        .build()
        ));

        scheduleRepository.save(ScheduleStub.newInstance()
                .withDate(LocalDate.now(clock))
                .addVisit(visit -> visit
                        .isVisited(true)
                        .withDoctor(doctor -> doctor.withId(ID)))
                .addVisit(visit -> visit
                        .isVisited(false)
                        .withDoctor(doctor -> doctor.withId(ID + 1)))
                .build());

        mvc.perform(MockMvcRequestBuilders
                        .get("/schedule/1"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.date", Matchers.is(LocalDate.now(clock).toString())),
                        MockMvcResultMatchers.jsonPath("$.doctors.size()", Matchers.is(2)),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].id", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].nickname", Matchers.is("Jão")),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].name", Matchers.is("João")),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].county", Matchers.is("Estreito")),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].district", Matchers.is("Centro")),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].expertise", Matchers.is("PEDIATRA")),
                        MockMvcResultMatchers.jsonPath("$.doctors[0].visited", Matchers.is(true)),

                        MockMvcResultMatchers.jsonPath("$.doctors[1].id", Matchers.is(2)),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].nickname", Matchers.is("Mary")),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].name", Matchers.is("Maria")),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].county", Matchers.is("Estreito")),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].district", Matchers.is("Centro")),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].expertise", Matchers.is("CLINICO GERAL")),
                        MockMvcResultMatchers.jsonPath("$.doctors[1].visited", Matchers.is(false))
                );
    }

    @Test
    void shouldThrowScheduleNotFoundWhenFindScheduleById() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .get("/schedule/1"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isNotFound(),
                        MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Schedule with id 1 not found"))
                );
    }
}
package com.iveso.doctorum.controller.doctor;

import com.iveso.doctorum.config.IntegrationTestBasics;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.is;

@Disabled
class DoctorControllerTest extends IntegrationTestBasics {

    @Test
    void shouldCreateDoctor() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "name": "Full name",
                            "nickname": "Nick",
                            "birth": "04/1985",
                            "contact": "123456789",
                            "email": "full@email.com",
                            "crm": "MA 654987",
                            "expertise": {"name": "Pediactric"},
                            "graduationYear": 2005,
                            "observation": "Some observation"
                        }
                        """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isCreated(),
                        MockMvcResultMatchers.jsonPath("$.id", is(1))
                );
    }
}
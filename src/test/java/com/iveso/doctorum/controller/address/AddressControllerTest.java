package com.iveso.doctorum.controller.address;

import com.iveso.doctorum.config.IntegrationTestBasics;
import com.iveso.doctorum.repository.CountyRepository;
import com.iveso.doctorum.repository.StateRepository;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class AddressControllerTest extends IntegrationTestBasics {

    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private CountyRepository countyRepository;

    @Test
    void shouldInflateStateByUfSuccessfully() throws Exception {
        String uf = "ma";

        wireMock.register(get(urlPathMatching("/ibge/localidades/estados"))
                .willReturn(ok()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("states.json")));

        mvc.perform(MockMvcRequestBuilders
                .patch("/addresses/fill/state/{uf}", uf)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpectAll(
                        MockMvcResultMatchers.status().isCreated(),
                        MockMvcResultMatchers.jsonPath("$.message", is("State %s save on DB successfully".formatted(uf.toUpperCase())))
                );
    }

    @Test
    void shouldInflateStatesOnDbSuccessfully() throws Exception {
        wireMock.register(get(urlPathMatching("/ibge/localidades/estados"))
                .willReturn(ok()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("states.json")));

        mvc.perform(MockMvcRequestBuilders
                .patch("/addresses/fill/states")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.message", is("All states inflate on DB successfully")));

        List<State> states = new ArrayList<>();
        stateRepository.findAll().forEach(states::add);
        assertEquals(3, states.size());
    }

    @Test
    void shouldThrowAlreadyStatesRegister() throws Exception {
        State state = new State();
        state.setId(123);
        state.setUf("TE");
        state.setName("Teste");

        stateRepository.save(state);

        mvc.perform(MockMvcRequestBuilders
                .patch("/addresses/fill/states")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpectAll(
                        MockMvcResultMatchers.status().isUnprocessableEntity(),
                        MockMvcResultMatchers.jsonPath("$.message", is("Already exists states resgister on DB.")));
    }

    @Test
    void shouldInflateCountiesFromUfSuccessfully() throws Exception {
        State state = new State();
        state.setId(123);
        state.setUf("TE");
        state.setName("Testing");

        wireMock.register(get(urlPathMatching("/ibge/localidades/estados/TE/municipios"))
                .willReturn(ok()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("counties.json")));

        stateRepository.save(state);

        mvc.perform(MockMvcRequestBuilders
                .patch("/addresses/fill/counties/state/{uf}", state.getUf())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.message", is("All counties from UF TE inflate on DB."))
                );

        List<County> counties = countyRepository.findAll();
        assertEquals(2, counties.size());
    }

    @Test
    void shouldThrowAlreadyExistsCountiesException() throws Exception {
        State state = new State();
        state.setId(123);
        state.setUf("TE");
        state.setName("Testing");
        stateRepository.save(state);

        County county = new County();
        county.setId(123);
        county.setName("Testing County");
        county.setState(state);
        countyRepository.save(county);

        mvc.perform(MockMvcRequestBuilders
                        .patch("/addresses/fill/counties/state/{uf}", state.getUf())
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpectAll(
                        MockMvcResultMatchers.status().isUnprocessableEntity(),
                        MockMvcResultMatchers.jsonPath("$.message", is("Already exists counties resgister from UF TE"))
                );
    }
}
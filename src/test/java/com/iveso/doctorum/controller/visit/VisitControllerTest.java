package com.iveso.doctorum.controller.visit;

import com.iveso.doctorum.config.IntegrationTestBasics;
import com.iveso.doctorum.repository.*;
import com.iveso.doctorum.stub.clinic.ClinicStub;
import com.iveso.doctorum.stub.clinic.CountyStub;
import com.iveso.doctorum.stub.doctor.DoctorStub;
import com.iveso.doctorum.stub.schedule.ScheduleStub;
import com.iveso.doctorum.stub.visit.VisitStub;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.time.LocalDate;

@Disabled
@SuppressWarnings("unused")
class VisitControllerTest extends IntegrationTestBasics {

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CountyRepository countyRepository;

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Test
    void shouldCheckVisit() throws Exception {
        visitRepository.save(VisitStub.newInstance()
                .isVisited(false)
                .build());

        mvc.perform(MockMvcRequestBuilders
                        .put("/visit/check")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id": 1,
                                    "visited": true
                                }
                                """))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void shouldThrowVisitNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .put("/visit/check")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id": 1,
                                    "visited": true
                                }
                                """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isNotFound(),
                        MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Visit with id 1 not found"))
                );
    }

    @Test
    void shouldFindVisitById() throws Exception {
        countyRepository.save(CountyStub.newInstance()
                .withId(1)
                .withName("Estreito")
                .build());

        clinicRepository.save(ClinicStub.newInstance()
                .withCounty(county -> county.withId(1))
                .withDistrict("Centro")
                .build());

        doctorRepository.save(DoctorStub.newInstance()
                .addClinic(clinic -> clinic.withId(1))
                .withExpertise("Pediatra")
                .withName("João Pedro")
                .withNickname("Jão")
                .build());

        scheduleRepository.save(ScheduleStub.newInstance()
                .addVisit(visit -> visit
                        .withDoctor(doctor -> doctor.withId(1))
                        .withObservation("Testando"))
                .withDate(LocalDate.parse("2024-01-10"))
                .build());

        mvc.perform(MockMvcRequestBuilders
                        .get("/visit/1"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)),
                        MockMvcResultMatchers.jsonPath("$.county", Matchers.is("Estreito")),
                        MockMvcResultMatchers.jsonPath("$.district", Matchers.is("Centro")),
                        MockMvcResultMatchers.jsonPath("$.expertise", Matchers.is("Pediatra")),
                        MockMvcResultMatchers.jsonPath("$.name", Matchers.is("João Pedro")),
                        MockMvcResultMatchers.jsonPath("$.nickname", Matchers.is("Jão")),
                        MockMvcResultMatchers.jsonPath("$.observation", Matchers.is("Testando")),
                        MockMvcResultMatchers.jsonPath("$.date", Matchers.is("2024-01-10"))
                );
    }

    @Test
    void shouldThrowVisitNotFoundWhenFindVisitById() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .get("/visit/1"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isNotFound(),
                        MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Visit with id 1 not found"))
                );
    }

    @Test
    void shouldRegisterObservation() throws Exception {
        visitRepository.save(VisitStub.newInstance()
                .isVisited(false)
                .build());

        mvc.perform(MockMvcRequestBuilders
                        .post("/visit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                        {
                            "visitId": 1,
                            "observation": "Testando"
                        }
                        """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1))
                );
    }

    @Test
    void shouldThrowVisitNotFoundWhenRegisterObservation() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/visit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                        {
                            "visitId": 1,
                            "observation": "Testando"
                        }
                        """))
                .andExpectAll(
                        MockMvcResultMatchers.status().isNotFound(),
                        MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Visit with id 1 not found"))
                );
    }

    @Test
    void shouldSaveVisitObservationAudio() throws Exception {
        visitRepository.save(VisitStub.newInstance()
                .isVisited(false)
                .build());

        URL resource = getClass().getClassLoader().getResource("__files/test.wav");
        assert resource != null;
        File file = new File(resource.toURI());

        FileInputStream input = new FileInputStream(file);
        MockMultipartFile upload= new MockMultipartFile("record", "filename.wav", "audio/wav", input);
        mvc.perform(MockMvcRequestBuilders
                        .multipart("/visit/file")
                        .file(upload)
                        .param("visitId", "1")
                        .param("observation", "teste"))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1))
                );
    }
}
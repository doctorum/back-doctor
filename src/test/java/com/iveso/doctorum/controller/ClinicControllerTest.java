package com.iveso.doctorum.controller;

import com.iveso.doctorum.config.IntegrationTestBasics;
import com.iveso.doctorum.repository.ClinicRepository;
import com.iveso.doctorum.repository.CountyRepository;
import com.iveso.doctorum.repository.StateRepository;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import com.iveso.doctorum.stub.clinic.ClinicStub;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.Matchers.is;

@Disabled
@SuppressWarnings("unused")
class ClinicControllerTest extends IntegrationTestBasics {
    @Autowired
    private ClinicRepository repository;

    @Autowired
    private CountyRepository countyRepository;

    @Autowired
    private StateRepository stateRepository;

    @Test
    void shouldCreateClinic() throws Exception {
        State state = new State();
        state.setId(2);
        state.setUf("MA");
        state.setName("Maranhão");
        state = stateRepository.save(state);

        County county = new County();
        county.setId(1);
        county.setName("Estreito");
        county.setState(state);
        county = countyRepository.save(county);

        mvc.perform(MockMvcRequestBuilders
                .post("/clinics")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "district": "Curto",
                            "location": "Clinica Brasil",
                            "publicPlace": "QD 07 Teste Rua 01",
                            "zipcode": 123456789,
                            "county": {
                                "id": %d
                            }
                        }
                        """.formatted(county.getId())))
                .andExpectAll(
                        MockMvcResultMatchers.status().isCreated(),
                        MockMvcResultMatchers.jsonPath("$.id", is(1))
                );
    }

    @Test
    @Transactional
    void shouldUpdateClinic() throws Exception {
        Clinic clinic = ClinicStub.newInstance()
                .withLocation("Test")
                .addDoctor(doctor -> doctor
                        .withName("Jonas Ferreira")
                        .withNickname("Jonas"))
                .build();

        clinic = repository.save(clinic);

        mvc.perform(MockMvcRequestBuilders
                .put("/clinics")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                          "id": %d,
                          "location": "Test",
                          "latitude": -14.654987114,
                          "longitude": -16.654987114
                        }
                        """.formatted(clinic.getId())))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        clinic = repository.findById(clinic.getId())
                .orElseGet(() -> Assertions.fail("Clinic must exists on base"));

        Assertions.assertNotNull(clinic.getDoctors(), "Clinic must have doctors saved on creation");
        Assertions.assertFalse(clinic.getDoctors().isEmpty(), "Clinic must have doctors saved on creation (empty)");
        Assertions.assertEquals("Test", clinic.getLocation());
        Assertions.assertEquals(-14.654987114, clinic.getLatitude());
        Assertions.assertEquals(-16.654987114, clinic.getLongitude());
    }

    @Test
    void shouldListAllClinics() throws Exception {
        State state = new State();
        state.setId(2);
        state.setUf("MA");
        state.setName("Maranhão");
        state = stateRepository.save(state);

        County county = new County();
        county.setId(1);
        county.setName("Estreito");
        county.setState(state);
        final int countyId = countyRepository.save(county).getId();

        county = new County();
        county.setId(3);
        county.setName("Imperatríz");
        county.setState(state);
        final int countyId3 = countyRepository.save(county).getId();

        state = new State();
        state.setId(3);
        state.setUf("GO");
        state.setName("Goias");
        state = stateRepository.save(state);

        county = new County();
        county.setId(4);
        county.setName("Cocalzinho");
        county.setState(state);
        final int countyId2 = countyRepository.save(county).getId();

        Clinic clinic1 = ClinicStub.newInstance()
                .withLocation("Clinica MA")
                .withDistrict("Brasil")
                .withContact(contact -> contact
                        .withContact(123456789))
                .withCounty(countyBuilder -> countyBuilder
                        .withId(countyId))
                .build();

        Clinic clinic2 = ClinicStub.newInstance()
                .withLocation("Clinica")
                .withDistrict("Centro")
                .withContact(contact -> contact
                        .withContact(45674125))
                .withCounty(countyBuilder -> countyBuilder
                        .withId(countyId3))
                .build();

        Clinic clinic3 = ClinicStub.newInstance()
                .withLocation("Clinica GO")
                .withDistrict("Brasil")
                .withContact(contact -> contact
                        .withContact(654987321))
                .withCounty(countyBuilder -> countyBuilder
                        .withId(countyId2))
                .build();

        repository.saveAll(List.of(clinic1, clinic2, clinic3));

        mvc.perform(MockMvcRequestBuilders
                .get("/clinics")
                .param("states", "2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.jsonPath("$.totalElements", is(2)),
                        MockMvcResultMatchers.jsonPath("$.content[0].location", is("Clinica MA")),
                        MockMvcResultMatchers.jsonPath("$.content[0].district", is("Brasil")),
                        MockMvcResultMatchers.jsonPath("$.content[0].contact", is(123456789)),
                        MockMvcResultMatchers.jsonPath("$.content[0].county.name", is("Estreito")),
                        MockMvcResultMatchers.jsonPath("$.content[0].county.state", is("MA")),

                        MockMvcResultMatchers.jsonPath("$.content[1].location", is("Clinica")),
                        MockMvcResultMatchers.jsonPath("$.content[1].district", is("Centro")),
                        MockMvcResultMatchers.jsonPath("$.content[1].contact", is(45674125)),
                        MockMvcResultMatchers.jsonPath("$.content[1].county.name", is("Imperatríz")),
                        MockMvcResultMatchers.jsonPath("$.content[1].county.state", is("MA"))
                );
    }
}
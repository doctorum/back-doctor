package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.clinic.request.UpdateClinicRequest;
import com.iveso.doctorum.repository.ClinicRepository;
import com.iveso.doctorum.repository.entity.Clinic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClinicServiceTest {

    @Mock
    private ClinicRepository repository;

    @InjectMocks
    private ClinicService service;

    @Test
    void shouldUpdateClinic() {
        UpdateClinicRequest request = new UpdateClinicRequest(
                1,
                "Test",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        Clinic clinic = mock(Clinic.class);

        when(repository.findById(request.id())).thenReturn(Optional.of(clinic));

        service.update(request);

        verify(clinic).setDistrict(request.district());
        verify(clinic).setLocation(request.location());
        verify(clinic).setPublicPlace(request.publicPlace());
        verify(clinic).setEmail(request.email());
        verify(clinic).setLatitude(request.latitude());
        verify(clinic).setLongitude(request.longitude());
    }
}
package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.visit.request.CreateVisitObservationRequest;
import com.iveso.doctorum.controller.visit.request.UpdateCheckVisitedDoctorRequest;
import com.iveso.doctorum.controller.visit.response.CreateVisitObservationResponse;
import com.iveso.doctorum.controller.visit.response.FindVisitResponse;
import com.iveso.doctorum.exception.visit.VisitNotFoundException;
import com.iveso.doctorum.repository.ScheduleRepository;
import com.iveso.doctorum.repository.VisitRepository;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.repository.projection.schedule.LocalDateOnly;
import com.iveso.doctorum.stub.visit.VisitStub;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VisitServiceTest {

    @InjectMocks
    private VisitService service;

    @Mock
    private VisitRepository repository;

    @Mock
    private ScheduleRepository scheduleRepository;

    @Test
    void shouldCheckVisit() {
        UpdateCheckVisitedDoctorRequest request = new UpdateCheckVisitedDoctorRequest(1, true);

        Visit visit = VisitStub.newInstance()
                .withId(1)
                .isVisited(false)
                .build();

        when(repository.findById(request.id())).thenReturn(Optional.of(visit));

        service.checkVisit(request);

        assertTrue(visit.isVisited());
    }

    @Test
    void shouldThrowVisitNotFoundOnCheckVisit() {
        UpdateCheckVisitedDoctorRequest request = new UpdateCheckVisitedDoctorRequest(1, true);

        when(repository.findById(request.id())).thenReturn(Optional.empty());

        assertThrows(VisitNotFoundException.class, () -> service.checkVisit(request));
    }

    @Test
    void shouldFindVisitById() {
        Visit visit = VisitStub.newInstance()
                .withId(1)
                .withDoctor(doctor -> doctor
                        .withName("João Pedro")
                        .withNickname("Pedro")
                        .withExpertise("Pediatra")
                        .addClinic(clinic -> clinic
                                .withDistrict("Centro")
                                .withCounty(county -> county
                                        .withName("Estreito"))
                        ))
                .build();

        LocalDate date = LocalDate.parse("2024-01-10");

        when(repository.findById(1)).thenReturn(Optional.of(visit));
        when(scheduleRepository.findByVisits(any(Visit.class))).thenReturn(new LocalDateOnly(date));

        FindVisitResponse response = service.findVisitById(1);

        assertEquals(visit.getId(), response.id());
        assertEquals(date, response.date());
        assertEquals(visit.getObservation(), response.observation());

        Doctor doctor = visit.getDoctor();
        assertEquals(doctor.getName(), response.name());
        assertEquals(doctor.getNickname(), response.nickname());
        assertEquals(doctor.getExpertise(), response.expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.district());
        assertEquals(clinic.getCounty().getName(), response.county());
    }

    @Test
    void shouldThrowVisitNotFoundWhenFindById() {
        when(repository.findById(1)).thenReturn(Optional.empty());
        assertThrows(VisitNotFoundException.class, () -> service.findVisitById(1));
    }

    @Test
    void shouldRegisterVisitObservation() {
        CreateVisitObservationRequest request = new CreateVisitObservationRequest(
                1,
                "Testando",
                null
        );

        Visit visit = VisitStub.newInstance()
                .withId(1)
                .build();

        when(repository.findById(request.visitId())).thenReturn(Optional.of(visit));

        CreateVisitObservationResponse response = service.registerObservation(request);

        assertEquals(visit.getId(), response.id());
        assertTrue(visit.isVisited());
        assertEquals(request.observation(), visit.getObservation());
    }

    @Test
    void shouldThrowVisitNotFoundWhenRegisterVisitObservation() {
        CreateVisitObservationRequest request = new CreateVisitObservationRequest(
                1,
                "Testando",
                null
        );

        when(repository.findById(request.visitId())).thenReturn(Optional.empty());

        assertThrows(VisitNotFoundException.class, () -> service.registerObservation(request));
    }

    @Test
    void shouldSaveVisitObservationAudio() throws Exception {
        URL resource = getClass().getClassLoader().getResource("__files/test.wav");
        File file = new File(resource.toURI());

        FileInputStream input = new FileInputStream(file);
        MockMultipartFile record = new MockMultipartFile("record", "filename.wav", "audio/wav", input);

        final int visitId = 1;
        final String observation = "Testando";

        Visit visit = VisitStub.newInstance()
                .withId(1)
                .build();

        when(repository.findById(visitId)).thenReturn(Optional.of(visit));

        CreateVisitObservationResponse response = service.registerObservation(visitId, observation, record);

        assertEquals(visit.getId(), response.id());
        assertTrue(visit.isVisited());
        assertEquals(observation, visit.getObservation());
        assertNotNull(visit.getAudioObservation());
        assertEquals(record.getBytes(), visit.getAudioObservation());
        assertNotNull(visit.getRecordName());
        assertEquals(record.getOriginalFilename(), visit.getRecordName());
    }
}
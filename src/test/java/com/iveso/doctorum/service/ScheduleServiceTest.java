package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.schedule.request.CreateScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.FilterScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.UpdateScheduleRequest;
import com.iveso.doctorum.controller.schedule.response.CreateScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.FindScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.ScheduleResponse;
import com.iveso.doctorum.exception.schedule.ScheduleNotFoundException;
import com.iveso.doctorum.repository.ScheduleRepository;
import com.iveso.doctorum.repository.VisitRepository;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.stub.schedule.ScheduleStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ScheduleServiceTest {

    @InjectMocks
    private ScheduleService service;

    @Mock
    private ScheduleRepository repository;

    @Mock
    private VisitRepository visitRepository;

    @Mock
    private Clock clock;

    private static final ZonedDateTime NOW = ZonedDateTime.of(
            2024, 1, 3,
            14, 28, 15, 0,
            ZoneId.of("GMT")
    );

    @BeforeEach
    void setup() {
        lenient().when(clock.getZone()).thenReturn(NOW.getZone());
        lenient().when(clock.instant()).thenReturn(NOW.toInstant());
    }

    @Test
    void shouldCreatesSchedule() {
        CreateScheduleRequest request = new CreateScheduleRequest(LocalDate.now(clock), List.of(1, 5, 6, 4, 7));

        when(repository.save(any(Schedule.class))).then(invocation -> {
            Schedule schedule = invocation.getArgument(0, Schedule.class);
            schedule.setId(1);

            return schedule;
        });

        CreateScheduleResponse response = service.createSchedule(request);

        assertEquals(1, response.id());
    }

    @Test
    void shouldUpdateScheduleAddingDoctors() {
        UpdateScheduleRequest request = new UpdateScheduleRequest(1, Arrays.asList(2, 3, 5));

        Schedule schedule = ScheduleStub.newInstance()
                .withId(request.id())
                .withDoctors(1, 2, 4, 5)
                .build();

        List<Visit> visits = spy(schedule.getVisits());
        schedule.setVisits(visits);

        when(repository.findById(request.id()))
                .thenReturn(Optional.of(schedule));

        service.updateSchedule(request);

        assertEquals(3, visits.size());
        verify(visits).removeIf(any());
        verify(visits).addAll(any(List.class));
        verify(visitRepository).deleteAll(any(List.class));
    }

    @Test
    void shouldUpdateScheduleRemovingDoctors() {
        UpdateScheduleRequest request = new UpdateScheduleRequest(1, List.of(1, 2, 5));

        Schedule schedule = ScheduleStub.newInstance()
                .withId(request.id())
                .withDoctors(1, 2, 4, 5)
                .build();

        when(repository.findById(request.id()))
                .thenReturn(Optional.of(schedule));

        service.updateSchedule(request);

        assertEquals(3, schedule.getVisits().size());
    }

    @Test
    void shouldThrowScheduleNotFoundWhenNotExists() {
        UpdateScheduleRequest request = new UpdateScheduleRequest(1, List.of(1, 3, 5));

        when(repository.findById(request.id()))
                .thenReturn(Optional.empty());

        assertThrows(ScheduleNotFoundException.class, () -> service.updateSchedule(request));
    }

    @Test
    void shouldListScheduleDays() {
        LocalDate date = LocalDate.now(clock);
        LocalDate from = date.withDayOfMonth(1);
        LocalDate to = date.withDayOfMonth(31);
        int month = 1;
        int year = 2024;

        when(repository.findByDateBetween(from, to)).thenReturn(List.of(
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(10)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(15)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(18)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(21)).build(),
                ScheduleStub.newInstance().withDate(date.withDayOfMonth(31)).build()
        ));

        List<Integer> daysWithSchedule = service.listScheduleDays(month, year);

        assertEquals(5, daysWithSchedule.size());
        assertEquals(10, daysWithSchedule.get(0));
        assertEquals(15, daysWithSchedule.get(1));
        assertEquals(18, daysWithSchedule.get(2));
        assertEquals(21, daysWithSchedule.get(3));
        assertEquals(31, daysWithSchedule.get(4));
    }

    @Test
    void shouldScheduleResponse() {
        FilterScheduleRequest request = new FilterScheduleRequest(
                LocalDate.now(clock),
                null,
                null,
                null
        );

        Schedule schedule = ScheduleStub.newInstance()
                .withId(1)
                .addVisit(visit -> visit
                        .withId(1)
                        .isVisited(true)
                        .withDoctor(doctor -> doctor
                                .withNickname("Jão")
                                .withName("João")
                                .withExpertise("PEDIATRA")
                                .addClinic(clinic -> clinic
                                        .withCounty(county -> county.withName("Estreito"))
                                        .withDistrict("Centro"))))
                .build();

        //noinspection unchecked
        when(repository.findOne(any(Specification.class))).thenReturn(Optional.of(schedule));

        ScheduleResponse response = service.findSchedules(request);

        assertEquals(schedule.getId(), response.id());
        assertEquals(schedule.getVisits().size(), response.visits().size());
        assertEquals(schedule.getVisits().get(0).isVisited(), response.visits().get(0).visited());

        Doctor doctor = schedule.getVisits().get(0).getDoctor();
        assertEquals(doctor.getName(), response.visits().get(0).name());
        assertEquals(doctor.getNickname(), response.visits().get(0).nickname());
        assertEquals(doctor.getExpertise(), response.visits().get(0).expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.visits().get(0).district());
        assertEquals(clinic.getCounty().getName(), response.visits().get(0).county());
    }

    @Test
    void shouldThrowScheduleNotFoundWhenFindSchedules() {
        FilterScheduleRequest request = new FilterScheduleRequest(
                LocalDate.now(clock),
                null,
                null,
                null
        );

        //noinspection unchecked
        when(repository.findOne(any(Specification.class))).thenReturn(Optional.empty());

        assertThrows(ScheduleNotFoundException.class, () -> service.findSchedules(request));
    }

    @Test
    void shouldFindScheduleResponse() {
        Schedule schedule = ScheduleStub.newInstance()
                .withId(1)
                .addVisit(visit -> visit
                        .withId(1)
                        .isVisited(true)
                        .withDoctor(doctor -> doctor
                                .withNickname("Jão")
                                .withName("João")
                                .withExpertise("PEDIATRA")
                                .addClinic(clinic -> clinic
                                        .withCounty(county -> county.withName("Estreito"))
                                        .withDistrict("Centro"))))
                .build();

        when(repository.findById(1)).thenReturn(Optional.of(schedule));

        FindScheduleResponse response = service.findScheduleById(1);

        assertEquals(schedule.getId(), response.id());
        assertEquals(schedule.getVisits().size(), response.doctors().size());
        assertEquals(schedule.getVisits().get(0).isVisited(), response.doctors().get(0).visited());

        Doctor doctor = schedule.getVisits().get(0).getDoctor();
        assertEquals(doctor.getName(), response.doctors().get(0).name());
        assertEquals(doctor.getNickname(), response.doctors().get(0).nickname());
        assertEquals(doctor.getExpertise(), response.doctors().get(0).expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.doctors().get(0).district());
        assertEquals(clinic.getCounty().getName(), response.doctors().get(0).county());
    }

    @Test
    void shouldThrowScheduleNotFoundWhenFindById() {
        when(repository.findById(1)).thenReturn(Optional.empty());
        assertThrows(ScheduleNotFoundException.class, () -> service.findScheduleById(1));
    }
}
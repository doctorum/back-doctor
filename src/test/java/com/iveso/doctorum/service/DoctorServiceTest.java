package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.doctor.request.CreateDoctor;
import com.iveso.doctorum.repository.DoctorRepository;
import com.iveso.doctorum.repository.entity.Doctor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DoctorServiceTest {

    @Mock
    private DoctorRepository repository;

    @Captor
    private ArgumentCaptor<Doctor> doctorCaptor;

    @InjectMocks
    private DoctorService service;

    @Test
    void shouldCreateDoctor() {
        CreateDoctor request = new CreateDoctor(
                "Test Complete",
                "Test",
                "04/1994",
                "test@email.com",
                "MA 123",
                "Some observation",
                123456789L,
                new CreateDoctor.Expertise("Pediatric"),
                2005,
                null
        );

        when(repository.save(any(Doctor.class))).thenAnswer(invocation -> {
            Doctor entity = invocation.getArgument(0, Doctor.class);
            entity.setId(1);
            return entity;
        });

        service.createDoctor(request);

        verify(repository).save(doctorCaptor.capture());
        Doctor entity = doctorCaptor.getValue();
        assertEquals(request.name(), entity.getName());
        assertEquals(request.nickname(), entity.getNickname());
        assertEquals(request.birth(), entity.getBirth());
        assertEquals(request.email(), entity.getEmail().getAddress());
        assertEquals(request.crm(), entity.getCrm());
        assertEquals(request.contact(), entity.getContact().getNumber());
        assertTrue(entity.getContact().isWhatsapp());
        assertEquals(request.expertise().name(), entity.getExpertise());
        assertEquals(request.graduationYear(), entity.getGraduationYear());

        assertEquals(1, entity.getId());
    }
}
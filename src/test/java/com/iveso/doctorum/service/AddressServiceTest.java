package com.iveso.doctorum.service;

import com.iveso.doctorum.client.IbgeClient;
import com.iveso.doctorum.client.response.CountyResponse;
import com.iveso.doctorum.client.response.StateResponse;
import com.iveso.doctorum.repository.CountyRepository;
import com.iveso.doctorum.repository.StateRepository;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AddressServiceTest {

    @InjectMocks
    private AddressService service;

    @Mock
    private IbgeClient ibgeClient;

    @Mock
    private StateRepository stateRepository;

    @Mock
    private CountyRepository countyRepository;

    @Test
    void shouldInflateStateByUF() {
        String uf = "MA";
        StateResponse stateResponse = new StateResponse(123, "Maranhão", "MA");

        when(stateRepository.findByUf(uf)).thenReturn(Optional.empty());
        when(ibgeClient.loadStates()).thenReturn(List.of(
                new StateResponse(456, "Goiás", "GO"),
                stateResponse
        ));

        service.fillStateByUf(uf);

        ArgumentCaptor<State> captor = ArgumentCaptor.forClass(State.class);
        verify(stateRepository).save(captor.capture());

        State state = captor.getValue();
        assertEquals(stateResponse.id(), state.getId());
        assertEquals(stateResponse.uf(), state.getUf());
        assertEquals(stateResponse.name(), state.getName());
    }

    @Test
    void shouldInflateStatesFromIBGE() {
        List<StateResponse> statesResponse = List.of(
                new StateResponse(456, "Goiás", "GO"),
                new StateResponse(123, "Maranhão", "MA")
        );

        when(ibgeClient.loadStates()).thenReturn(statesResponse);

        service.fillStates();

        ArgumentCaptor<List<State>> captor = ArgumentCaptor.forClass(List.class);
        verify(stateRepository).saveAll(captor.capture());

        List<State> states = captor.getValue();
        assertEquals(statesResponse.size(), states.size());
        assertEquals(statesResponse.get(0).id(), states.get(0).getId());
        assertEquals(statesResponse.get(0).uf(), states.get(0).getUf());
        assertEquals(statesResponse.get(0).name(), states.get(0).getName());

        assertEquals(statesResponse.get(1).id(), states.get(1).getId());
        assertEquals(statesResponse.get(1).uf(), states.get(1).getUf());
        assertEquals(statesResponse.get(1).name(), states.get(1).getName());
    }

    @Test
    void shouldFindAllStates() {
        when(stateRepository.findAll()).thenReturn(List.of(mock(State.class)));

        List<com.iveso.doctorum.controller.address.response.StateResponse> states = service.findAll();

        assertEquals(1, states.size());
    }

    @Test
    void shouldInflateCountiesByState() {
        String uf = "GO";
        List<CountyResponse> countiesResponse = List.of(
                new CountyResponse(654, "Águas Lindas"),
                new CountyResponse(789, "Abadiânia")
        );

        State state = mock(State.class);

        when(stateRepository.findByUf(uf)).thenReturn(Optional.of(state));
        when(countyRepository.countByState(any(State.class))).thenReturn(0L);
        when(ibgeClient.loadCounties(uf)).thenReturn(countiesResponse);

        service.fillCounties(uf);

        ArgumentCaptor<List<County>> captor = ArgumentCaptor.forClass(List.class);
        verify(countyRepository).saveAll(captor.capture());

        List<County> entities = captor.getValue();
        assertEquals(countiesResponse.size(), entities.size());
        assertEquals(countiesResponse.get(0).id(), entities.get(0).getId());
        assertEquals(countiesResponse.get(0).name(), entities.get(0).getName());
        assertEquals(state, entities.get(0).getState());

        assertEquals(countiesResponse.get(1).id(), entities.get(1).getId());
        assertEquals(countiesResponse.get(1).name(), entities.get(1).getName());
        assertEquals(state, entities.get(1).getState());
    }
}
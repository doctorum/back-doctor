package com.iveso.doctorum.mapper.address;

import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountyMapperTest {

    @Test
    void shouldCreateCountyResponseFromCountyEntity() {
        State state = new State();
        state.setId(654987);
        state.setUf("TE");
        state.setName("Testing State");

        County county = new County();
        county.setId(123456);
        county.setName("Testing");
        county.setState(state);

        CountyResponse response = CountyMapper.INSTANCE.from(county);

        assertEquals(state.getId(), response.state().id());
        assertEquals(state.getUf(), response.state().uf());
        assertEquals(state.getName(), response.state().name());

        assertEquals(county.getId(), response.id());
        assertEquals(county.getName(), response.name());
    }

    @Test
    void shouldCreateCountyEneityFromCountyResponse() {
        com.iveso.doctorum.client.response.CountyResponse response = new com.iveso.doctorum.client.response.CountyResponse(123, "Testing");

        County entity = CountyMapper.INSTANCE.toEntity(response);

        assertEquals(response.id(), entity.getId());
        assertEquals(response.name(), entity.getName());
    }
}
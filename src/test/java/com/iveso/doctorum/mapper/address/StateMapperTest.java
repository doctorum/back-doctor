package com.iveso.doctorum.mapper.address;

import com.iveso.doctorum.client.response.StateResponse;
import com.iveso.doctorum.repository.entity.State;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StateMapperTest {

    @Test
    void shouldCreateStateFromStateResponse() {
        StateResponse response = new StateResponse(
                123456,
                "Goiás",
                "GO"
        );

        State state = StateMapper.INSTANCE.from(response);

        assertEquals(response.id(), state.getId());
        assertEquals(response.uf(), state.getUf());
        assertEquals(response.name(), state.getName());
    }

    @Test
    void shouldCreateStateResponseFromState() {
        State state = new State();
        state.setId(123);;
        state.setUf("TE");
        state.setName("Testing");

        com.iveso.doctorum.controller.address.response.StateResponse response = StateMapper.INSTANCE.from(state);

        assertEquals(state.getId(), response.id());
        assertEquals(state.getUf(), response.uf());
        assertEquals(state.getName(), response.name());
    }
}
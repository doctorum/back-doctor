package com.iveso.doctorum.mapper.doctor;

import com.iveso.doctorum.controller.doctor.request.CreateDoctor;
import com.iveso.doctorum.controller.doctor.request.UpdateDoctorRequest;
import com.iveso.doctorum.controller.doctor.response.DoctorDetailResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.stub.doctor.DoctorStub;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DoctorMapperTest {

    @Test
    void shouldCreateDoctorResponse() {
        Doctor doctor = new Doctor();
        doctor.setId(1);
        doctor.setExpertise("pediatra");
        doctor.setName("Doctor Test");
        doctor.setNickname("Doctor");

        DoctorResponse response = DoctorMapper.INSTANCE.createResponse(doctor);

        assertEquals(doctor.getId(), response.id());
        assertEquals(doctor.getExpertise(), response.expertise());
        assertEquals(doctor.getName(), response.name());
        assertEquals(doctor.getNickname(), response.nickname());
    }

    @Test
    void shouldCreateDoctorEntityFromCreateDoctorRequest() {
        CreateDoctor request = new CreateDoctor(
                "Test Complete",
                "Test",
                "04/1994",
                "test@email.com",
                "MA 123",
                "Some observation",
                123456789L,
                new CreateDoctor.Expertise("Pediatric"),
                2005,
                List.of(1, 2)
        );

        Doctor entity = DoctorMapper.INSTANCE.createEntityFrom(request);

        assertEquals(request.name(), entity.getName());
        assertEquals(request.nickname(), entity.getNickname());
        assertEquals(request.birth(), entity.getBirth());
        assertEquals(request.email(), entity.getEmail().getAddress());
        assertEquals(request.crm(), entity.getCrm());
        assertEquals(request.contact(), entity.getContact().getNumber());
        assertTrue(entity.getContact().isWhatsapp());
        assertEquals(request.expertise().name(), entity.getExpertise());
        assertEquals(request.graduationYear(), entity.getGraduationYear());
        assertEquals(2, entity.getClinics().size());
        assertEquals(1, entity.getClinics().get(0).getId());
        assertEquals(2, entity.getClinics().get(1).getId());
    }

    @Test
    void shouldCreateDoctorEntityFromCreateDoctorRequestWithoutClinics() {
        CreateDoctor request = new CreateDoctor(
                "Test Complete",
                "Test",
                "04/1994",
                "test@email.com",
                "MA 123",
                "Some observation",
                123456789L,
                new CreateDoctor.Expertise("Pediatric"),
                2005,
                null
        );

        Doctor entity = DoctorMapper.INSTANCE.createEntityFrom(request);

        assertEquals(request.name(), entity.getName());
        assertEquals(request.nickname(), entity.getNickname());
        assertEquals(request.birth(), entity.getBirth());
        assertEquals(request.email(), entity.getEmail().getAddress());
        assertEquals(request.crm(), entity.getCrm());
        assertEquals(request.contact(), entity.getContact().getNumber());
        assertTrue(entity.getContact().isWhatsapp());
        assertEquals(request.expertise().name(), entity.getExpertise());
        assertEquals(request.graduationYear(), entity.getGraduationYear());
        assertEquals(0, entity.getClinics().size());
    }

    @Test
    void shouldCreateDoctorEntityFromUpdateDoctorRequest() {
        UpdateDoctorRequest request = new UpdateDoctorRequest(
                1,
                "Test Complete",
                "Test",
                "04/1994",
                "test@email.com",
                "MA 123",
                "Some observation",
                123456789L,
                new UpdateDoctorRequest.Expertise("Pediatric"),
                2005,
                List.of(1, 2)
        );

        Doctor entity = DoctorMapper.INSTANCE.createEntityFrom(request);

        assertEquals(request.id(), entity.getId());
        assertEquals(request.name(), entity.getName());
        assertEquals(request.nickname(), entity.getNickname());
        assertEquals(request.birth(), entity.getBirth());
        assertEquals(request.email(), entity.getEmail().getAddress());
        assertEquals(request.crm(), entity.getCrm());
        assertEquals(request.contact(), entity.getContact().getNumber());
        assertTrue(entity.getContact().isWhatsapp());
        assertEquals(request.expertise().name(), entity.getExpertise());
        assertEquals(request.graduationYear(), entity.getGraduationYear());
        assertEquals(2, entity.getClinics().size());
    }

    @Test
    void shouldCreateDoctorDetailResponse() {
        Doctor doctor = DoctorStub.newInstance()
                .withId(1)
                .withName("João")
                .withNickname("Jota")
                .withExpertise("Pediatra")
                .withBirth("03/1995")
                .withCrm("123456 MA")
                .withSector("1ª Semana")
                .withGraduationYear(2010)
                .withContact(contact -> contact.withContact(99999999999L))
                .withEmail(email -> email.withAddress("joao@clinica.com.br"))
                .addClinic(clinic -> clinic.withId(1)
                        .withLocation("Clínica Geral")
                        .withCounty(county -> county
                                .withName("Estreito")
                                .withState(state -> state.withUf("MA"))))
                .build();

        DoctorDetailResponse response = DoctorMapper.INSTANCE.doctorDetailResponse(doctor);

        assertEquals(doctor.getId(), response.id());
        assertEquals(doctor.getName(), response.name());
        assertEquals(doctor.getNickname(), response.nickname());
        assertEquals(doctor.getExpertise(), response.expertise());
        assertEquals(doctor.getBirth(), response.birth());
        assertEquals(doctor.getCrm(), response.crm());
        assertEquals(doctor.getSector(), response.sector());
        assertEquals(doctor.getGraduationYear(), response.graduationYear());
        assertEquals(doctor.getContact().getNumber(), response.contact().number());
        assertEquals(doctor.getEmail().getAddress(), response.contact().email());
        assertEquals(doctor.getClinics().size(), response.clinics().size());

        Clinic clinic = doctor.getClinics().get(0);
        DoctorDetailResponse.ClinicResponse clinicResponse = response.clinics().get(0);
        assertEquals(clinic.getId(), clinicResponse.id());
        assertEquals(clinic.getLocation(), clinicResponse.name());
        assertEquals(clinic.getCounty().getName(), clinicResponse.county());
        assertEquals(clinic.getCounty().getState().getUf(), clinicResponse.uf());
    }
}
package com.iveso.doctorum.mapper.clinic;

import com.iveso.doctorum.controller.clinic.request.CreateClinic;
import com.iveso.doctorum.controller.clinic.response.ClinicDetailResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicIdResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.stub.clinic.ClinicStub;
import com.iveso.doctorum.stub.clinic.CreateClinicRequestStub;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClinicMapperTest {

    @Test
    void shouldCreateClinicEntity() {
        CreateClinic createClinic = CreateClinicRequestStub.newInstance()
                .withDistrict("Bairro")
                .withLocation("Clinica Brasil")
                .withZipcode(123456)
                .withPublicPlace("Qd B1")
                .withEmail("clinica.brasil@saude.com.br")
                .withPhone(99999998888L)
                .withLatitude(-15.7351936)
                .withLongitude(48.2869248)
                .withCounty(county -> county
                        .withId(1)
                        .withName("Estreito")
                        .withState(state -> state
                                .withId(2)
                                .withName("Maranhão")
                                .withUf("MA")))
                .build();

        Clinic entity = ClinicMapper.INSTANCE.createEntityFrom(createClinic);

        assertEquals(createClinic.district(), entity.getDistrict());
        assertEquals(createClinic.location(), entity.getLocation());
        assertEquals(createClinic.zipcode(), entity.getZipcode());
        assertEquals(createClinic.publicPlace(), entity.getPublicPlace());
        assertEquals(createClinic.email(), entity.getEmail());
        assertEquals(createClinic.phone(), entity.getContact().getNumber());
        assertEquals(createClinic.latitude().doubleValue(), entity.getLatitude().doubleValue());
        assertEquals(createClinic.longitude().doubleValue(), entity.getLongitude().doubleValue());
        assertEquals(createClinic.county().id(), entity.getCounty().getId());
        assertEquals(createClinic.county().name(), entity.getCounty().getName());
        assertEquals(createClinic.county().state().id(), entity.getCounty().getState().getId());
        assertEquals(createClinic.county().state().name(), entity.getCounty().getState().getName());
        assertEquals(createClinic.county().state().uf(), entity.getCounty().getState().getUf());
    }

    @Test
    void shouldCreateClinicResponse() {
        Clinic clinic = ClinicStub.newInstance()
                .withLocation("Clinica teste")
                .withDistrict("MA")
                .withContact(contact -> contact
                        .withContact(123456))
                .withCounty(county -> county
                        .withName("Estreito")
                        .withState(state -> state.withUf("MA")))
                .build();

        ClinicResponse response = ClinicMapper.INSTANCE.createResponse(clinic);

        assertEquals(clinic.getId(), response.id());
        assertEquals(clinic.getContact().getNumber(), response.contact());
        assertEquals(clinic.getLocation(), response.location());
        assertEquals(clinic.getDistrict(), response.district());
        assertEquals(clinic.getCounty().getName(), response.county().name());
        assertEquals(clinic.getCounty().getState().getUf(), response.county().state());
    }

    @Test
    void shouldCreateClinicDetailResponse() {
        Clinic clinic = ClinicStub.newInstance()
                .withId(1)
                .withLocation("Clinica Teste")
                .withPublicPlace("Qd 30")
                .withDistrict("Centro")
                .withContact(contact -> contact
                        .withContact(123456))
                .withCounty(county -> county
                        .withName("Estreito")
                        .withState(state -> state.withUf("MA")))
                .build();

        ClinicDetailResponse response = ClinicMapper.INSTANCE.clinicDetailResponse(clinic);

        assertEquals(clinic.getId(), response.id());
        assertEquals(clinic.getLocation(), response.name());
        assertEquals(clinic.getPublicPlace(), response.publicPlace());
        assertEquals(clinic.getDistrict(), response.district());
        assertEquals(clinic.getCounty().getName(), response.county());
        assertEquals(clinic.getCounty().getState().getUf(), response.uf());
        assertEquals(clinic.getContact().getNumber(), response.contact());
    }

    @Test
    void shouldCreateClinicIdResponse() {
        Clinic clinic = ClinicStub.newInstance()
                .withId(1)
                .withLocation("Clinica Teste")
                .withPublicPlace("Qd 30")
                .withDistrict("Centro")
                .withEmail("teste@email.com")
                .withZipcode(654321987)
                .withLatitude(-14.32165478)
                .withLongitude(-15.65432178)
                .withContact(contact -> contact
                        .withContact(123456))
                .withCounty(county -> county
                        .withName("Estreito")
                        .withState(state -> state.withUf("MA")))
                .build();

        ClinicIdResponse response = ClinicMapper.INSTANCE.clinicResponse(clinic);

        assertEquals(clinic.getId(), response.id());
        assertEquals(clinic.getLocation(), response.name());
        assertEquals(clinic.getPublicPlace(), response.publicPlace());
        assertEquals(clinic.getDistrict(), response.district());
        assertEquals(clinic.getEmail(), response.email());
        assertEquals(clinic.getContact().getNumber(), response.phone());
        assertEquals(clinic.getZipcode(), response.zipcode());
        assertEquals(clinic.getLatitude(), response.latitude());
        assertEquals(clinic.getLongitude(), response.longitude());

        County county = clinic.getCounty();
        ClinicIdResponse.County countyResponse = response.county();
        assertEquals(county.getId(), countyResponse.id());
        assertEquals(county.getName(), countyResponse.name());
        assertEquals(county.getState().getUf(), countyResponse.state().uf());
    }
}
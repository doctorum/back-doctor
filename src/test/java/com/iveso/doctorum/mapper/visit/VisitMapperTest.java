package com.iveso.doctorum.mapper.visit;

import com.iveso.doctorum.controller.visit.request.CreateVisitObservationRequest;
import com.iveso.doctorum.controller.visit.response.FindVisitResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.stub.visit.VisitStub;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VisitMapperTest {

    @Test
    void shouldFindVisitResponse() {
        Visit visit = VisitStub.newInstance()
                .withId(1)
                .withDoctor(doctor -> doctor
                        .withName("João Pedro")
                        .withNickname("Pedro")
                        .withExpertise("Pediatra")
                        .addClinic(clinic -> clinic
                                .withDistrict("Centro")
                                .withCounty(county -> county
                                        .withName("Estreito"))
                        ))
                .build();

        LocalDate date = LocalDate.parse("2024-01-10");

        FindVisitResponse response = VisitMapper.INSTANCE.toFindVisitResponse(visit, date);

        assertEquals(visit.getId(), response.id());
        assertEquals(date, response.date());
        assertEquals(visit.getObservation(), response.observation());

        Doctor doctor = visit.getDoctor();
        assertEquals(doctor.getName(), response.name());
        assertEquals(doctor.getNickname(), response.nickname());
        assertEquals(doctor.getExpertise(), response.expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.district());
        assertEquals(clinic.getCounty().getName(), response.county());
    }

    @Test
    void shouldCreateVisitObservationRequest() throws Exception {
        final int id = 1;
        final String observation = "Teste";

        URL resource = getClass().getClassLoader().getResource("__files/test.wav");
        File file = new File(resource.toURI());

        FileInputStream input = new FileInputStream(file);
        MultipartFile record = new MockMultipartFile("record", "filename.wav", "audio/wav", input);;
        CreateVisitObservationRequest request = VisitMapper.INSTANCE.toCreateVisitObservationRequest(id, observation, record);

        assertEquals(id, request.visitId());
        assertEquals(observation, request.observation());
        assertEquals(record.getOriginalFilename(), request.record().title());
        assertEquals(record.getSize(), request.record().record().length);
    }
}
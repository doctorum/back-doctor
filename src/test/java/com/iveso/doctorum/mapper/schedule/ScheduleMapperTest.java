package com.iveso.doctorum.mapper.schedule;

import com.iveso.doctorum.controller.schedule.request.CreateScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.UpdateScheduleRequest;
import com.iveso.doctorum.controller.schedule.response.FindScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.ScheduleResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.stub.schedule.ScheduleStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ScheduleMapperTest {

    @Mock
    private Clock clock;

    private static final ZonedDateTime NOW = ZonedDateTime.of(
            2024, 1, 3,
            13, 21, 0, 0,
            ZoneId.of("GMT")
    );


    @BeforeEach
    void setup() {
        lenient().when(clock.getZone()).thenReturn(NOW.getZone());
        lenient().when(clock.instant()).thenReturn(NOW.toInstant());
    }

    @Test
    void shouldCreatesScheduleEntityFromRequest() {
        CreateScheduleRequest request = new CreateScheduleRequest(
                LocalDate.now(clock),
                List.of(1, 5, 6, 3, 4)
        );

        Schedule schedule = ScheduleMapper.INSTANCE.toScheduleEntity(request);

        assertEquals(request.date().getDayOfMonth(), schedule.getDate().getDayOfMonth());
        assertEquals(request.date().getMonth(), schedule.getDate().getMonth());
        assertEquals(request.date().getYear(), schedule.getDate().getYear());
        assertEquals(request.doctors().size(), schedule.getVisits().size());
    }

    @Test
    void shouldUpdateScheduleEntityFromRequest() {
        UpdateScheduleRequest request = new UpdateScheduleRequest(
                1,
                List.of(1, 5, 6, 3, 4)
        );

        Schedule schedule = ScheduleMapper.INSTANCE.toScheduleEntity(request);

        assertEquals(request.id(), schedule.getId());
        assertEquals(request.doctors().size(), schedule.getVisits().size());
    }

    @Test
    void shouldScheduleResponseFromSchedule() {
        Schedule schedule = ScheduleStub.newInstance()
                .withId(1)
                .addVisit(visit -> visit
                        .withId(1)
                        .isVisited(true)
                        .withDoctor(doctor -> doctor
                                .withNickname("Jão")
                                .withName("João")
                                .withExpertise("PEDIATRA")
                                .addClinic(clinic -> clinic
                                        .withCounty(county -> county.withName("Estreito"))
                                        .withDistrict("Centro"))))
                .build();

        ScheduleResponse response = ScheduleMapper.INSTANCE.toScheduleResponse(schedule);

        assertEquals(schedule.getId(), response.id());
        assertEquals(schedule.getVisits().size(), response.visits().size());
        assertEquals(schedule.getVisits().get(0).isVisited(), response.visits().get(0).visited());

        Doctor doctor = schedule.getVisits().get(0).getDoctor();
        assertEquals(doctor.getName(), response.visits().get(0).name());
        assertEquals(doctor.getNickname(), response.visits().get(0).nickname());
        assertEquals(doctor.getExpertise(), response.visits().get(0).expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.visits().get(0).district());
        assertEquals(clinic.getCounty().getName(), response.visits().get(0).county());
    }

    @Test
    void shouldFindScheduleResponseFromSchedule() {
        Schedule schedule = ScheduleStub.newInstance()
                .withId(1)
                .addVisit(visit -> visit
                        .withId(1)
                        .isVisited(true)
                        .withDoctor(doctor -> doctor
                                .withNickname("Jão")
                                .withName("João")
                                .withExpertise("PEDIATRA")
                                .addClinic(clinic -> clinic
                                        .withCounty(county -> county.withName("Estreito"))
                                        .withDistrict("Centro"))))
                .build();

        FindScheduleResponse response = ScheduleMapper.INSTANCE.toFindScheduleResponse(schedule);

        assertEquals(schedule.getId(), response.id());
        assertEquals(schedule.getVisits().size(), response.doctors().size());
        assertEquals(schedule.getVisits().get(0).isVisited(), response.doctors().get(0).visited());

        Doctor doctor = schedule.getVisits().get(0).getDoctor();
        assertEquals(doctor.getId(), response.doctors().get(0).id());
        assertEquals(doctor.getName(), response.doctors().get(0).name());
        assertEquals(doctor.getNickname(), response.doctors().get(0).nickname());
        assertEquals(doctor.getExpertise(), response.doctors().get(0).expertise());

        Clinic clinic = doctor.getClinics().get(0);
        assertEquals(clinic.getDistrict(), response.doctors().get(0).district());
        assertEquals(clinic.getCounty().getName(), response.doctors().get(0).county());
    }
}
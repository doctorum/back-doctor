package com.iveso.doctorum.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocalDateUtilTest {

    private static final int MONTH = 1;
    private static final int YEAR = 2024;

    @Test
    void shouldGetFirstDateOfMonth() {
        LocalDate firstDate = LocalDateUtil.firstDate(MONTH, YEAR);
        assertEquals("2024-01-01", firstDate.toString());
    }

    @Test
    void shouldGetLastDateOfMonth() {
        LocalDate lastDate = LocalDateUtil.lastDate(MONTH, YEAR);
        assertEquals("2024-01-31", lastDate.toString());
    }

    @Test
    void shouldGetLastDateOfFebruaryMonth() {
        LocalDate firstDate = LocalDateUtil.lastDate(2, YEAR);
        assertEquals("2024-02-29", firstDate.toString());
    }
}
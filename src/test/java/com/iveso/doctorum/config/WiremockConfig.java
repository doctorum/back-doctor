package com.iveso.doctorum.config;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class WiremockConfig {

    @Bean
    public WireMock wireMock(@Value("${wiremock.server.port}") Integer port) {
        return WireMock.create()
                .port(port)
                .build();
    }

}

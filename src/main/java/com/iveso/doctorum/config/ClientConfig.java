package com.iveso.doctorum.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.List;

@Configuration
public class ClientConfig {

    @Bean
    public RestTemplate ibgeResource(@Value("${app.services.ibge}") final String baseUrl, RestTemplateBuilder builder) {
        return builder
                .uriTemplateHandler(new DefaultUriBuilderFactory(baseUrl))
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer(@Value("${cors.origins}") String... origins) {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins(origins);
            }
        };
    }
}

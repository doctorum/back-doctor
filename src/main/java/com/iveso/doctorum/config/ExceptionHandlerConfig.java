package com.iveso.doctorum.config;

import com.iveso.doctorum.exception.platform.BusinessException;
import com.iveso.doctorum.exception.converter.ErrorMessageConverter;
import com.iveso.doctorum.exception.converter.HttpStatusExceptionConverter;
import com.iveso.doctorum.exception.response.ErrorMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerConfig {
    private final HttpStatusExceptionConverter httpConverter = new HttpStatusExceptionConverter();
    private final ErrorMessageConverter messageConverter = new ErrorMessageConverter();
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorMessage> handleBusinessException(Throwable businessException) {
        return ResponseEntity
                .status(httpConverter.convert(businessException))
                .body(messageConverter.convert(businessException));
    }
}

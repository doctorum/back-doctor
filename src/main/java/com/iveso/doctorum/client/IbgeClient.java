package com.iveso.doctorum.client;

import com.iveso.doctorum.client.response.CountyResponse;
import com.iveso.doctorum.client.response.StateResponse;
import com.iveso.doctorum.exception.platform.BusinessException;
import com.iveso.doctorum.exception.platform.TechnicalException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class IbgeClient {

    private final RestTemplate ibgeResource;

    public List<StateResponse> loadStates() {
        try {
            log.info("Requesting states from IBGE API");
            List<StateResponse> states = List.of(ibgeResource
                    .getForEntity("/localidades/estados", StateResponse[].class)
                    .getBody());

            log.info("States from IBGE finded: Response: {}", states);
            return states;
        } catch(RestClientException exception) {
            log.error("An error has occurred on loading states from IBGE API. Message: {}", exception.getMessage(), exception);
            throw new TechnicalException("Not possible load states from IBGE API", exception);
        }
    }

    public List<CountyResponse> loadCounties(String uf) {
        try {
            log.info("Requesting counties from UF {} on IBGE API", uf);
            List<CountyResponse> counties = List.of(ibgeResource
                    .getForEntity("/localidades/estados/{UF}/municipios", CountyResponse[].class, uf)
                    .getBody());

            log.info("Counties from UF {} on IBGE finded: Response: {}", uf, counties);
            return counties;
        } catch(RestClientException exception) {
            log.error("An error has occurred on loading counties from IBGE API. Message: {}", exception.getMessage(), exception);
            throw new TechnicalException("Not possible load states from IBGE API", exception);
        }
    }
}

package com.iveso.doctorum.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record StateResponse(
        Integer id,
        @JsonProperty("nome")
        String name,
        @JsonProperty("sigla")
        String uf) {
}

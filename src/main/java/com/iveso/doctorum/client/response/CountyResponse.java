package com.iveso.doctorum.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CountyResponse(
        Integer id,
        @JsonProperty("nome")
        String name
) {
}

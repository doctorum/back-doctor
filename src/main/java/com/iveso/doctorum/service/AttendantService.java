package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.attendant.response.AttendantResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor

public class AttendantService {
    public List<AttendantResponse> findAttendantsByClinic(Integer id) {
        log.info("Searching attendants from clinic with id {}.", id);
        List<AttendantResponse> attendants = List.of();

        log.info("Attendants from clinic with id {} found. {}", id, attendants);
        return attendants;
    }
}

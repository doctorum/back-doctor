package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.controller.doctor.request.CreateDoctor;
import com.iveso.doctorum.controller.doctor.request.FilterDoctorRequest;
import com.iveso.doctorum.controller.doctor.request.UpdateDoctorRequest;
import com.iveso.doctorum.controller.doctor.response.CreateDoctorResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorDetailResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorResponse;
import com.iveso.doctorum.controller.doctor.response.ExpertiseResponse;
import com.iveso.doctorum.exception.doctor.DoctorNotFoundException;
import com.iveso.doctorum.exception.platform.TechnicalException;
import com.iveso.doctorum.mapper.address.CountyMapper;
import com.iveso.doctorum.mapper.doctor.DoctorMapper;
import com.iveso.doctorum.repository.DoctorRepository;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.filter.DoctorFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DoctorService {
    private final DoctorRepository repository;

    public Page<DoctorResponse> findAll(FilterDoctorRequest request, PageRequest pageRequest) {
        log.info("Finding all doctors from page {} and size {} with filters {}", pageRequest.getPageNumber(), pageRequest.getPageSize(), request);
        Page<Doctor> page = repository.findAll(
                DoctorFilter.newInstance()
                        .withSectors(request.sectors())
                        .withExpertises(request.expertises())
                        .withCounties(request.counties()),
                pageRequest.withSort(Sort.Direction.ASC, "nickname", "name"));

        List<Doctor> doctors = page.toList();

        log.info("Doctors found. {}", doctors);

        return page.map(DoctorMapper.INSTANCE::createResponse);
    }

    @Transactional
    public DoctorDetailResponse detailDoctor(Integer doctorId) {
        try {
            log.info("Finding detail doctor id {}", doctorId);

            Doctor doctor = repository.findById(doctorId).orElseThrow(() -> new DoctorNotFoundException(doctorId));
            log.info("Doctor details with id {} found. {}", doctorId, doctor);

            return DoctorMapper.INSTANCE.doctorDetailResponse(doctor);
        } catch(Exception e) {
            log.error("An error has occurred on find doctor details with id {}. Message: {}", doctorId, e.getMessage(), e);
            return null;
        }
    }

    public List<DoctorResponse> findByClinic(Integer id) {
        log.info("Searching doctors from clinic with id {}", id);
        List<Doctor> doctors = repository.findAll(DoctorFilter.newInstance()
                .withClinicId(id));

        log.info("Doctors from clinic with id {} found. {}", id, doctors);
        return doctors.stream()
                .map(DoctorMapper.INSTANCE::createResponse)
                .toList();
    }

    public List<ExpertiseResponse> findExpertises() {
        log.info("Searching all expertises");
        List<String> expertises = List.of(
                "Cardiologista",
                "Clínico Geral",
                "Dermatologista",
                "Enfermeiro",
                "Gastro",
                "Ginecologia",
                "Nutricionista",
                "Ortopedista",
                "Pediatra");

        log.info("Expertises founed. {}", expertises);
        return expertises.stream().map(ExpertiseResponse::new).toList();
    }

    public CreateDoctorResponse createDoctor(CreateDoctor request) {
        try {
            log.info("Creating doctor from request. {}", request);
            Doctor entity = DoctorMapper.INSTANCE.createEntityFrom(request);

            log.info("Saving doctor entity: {}", entity);
            entity = repository.save(entity);

            CreateDoctorResponse response = new CreateDoctorResponse(entity.getId());
            log.info("Doctor created. {}", response);

            return response;
        } catch(IllegalArgumentException exception) {
            log.error("An error has occurred on register doctor from request {}. Message: {}", request, exception.getMessage(), exception);
            throw new TechnicalException("Was not possible create clinic", exception);
        }
    }

    public List<String> findSectors() {
        log.info("Finding all sectors from doctors");
        List<String> sectors = repository.findSectors();

        log.info("Sectors found. {}", sectors);
        return sectors;
    }

    public List<CountyResponse> findCounties() {
        log.info("Finding all counties from doctors");
        List<County> counties = repository.findCountiesFromDoctors();

        log.info("Counties from doctors found. {}", counties);
        return counties.stream()
                .map(CountyMapper.INSTANCE::from)
                .toList();
    }

    public void updateDoctor(UpdateDoctorRequest request) {
        log.info("Updating doctor with id {}. Payload: {}", request.id(), request);
        Doctor entity = DoctorMapper.INSTANCE.createEntityFrom(request);

        repository.save(entity);
        log.info("Doctor with id {} updated.", request.id());
    }
}

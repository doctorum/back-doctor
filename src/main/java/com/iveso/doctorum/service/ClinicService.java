package com.iveso.doctorum.service;

import com.iveso.doctorum.client.IbgeClient;
import com.iveso.doctorum.controller.clinic.request.CreateClinic;
import com.iveso.doctorum.controller.clinic.request.FilterClinicsRequest;
import com.iveso.doctorum.controller.clinic.request.UpdateClinicRequest;
import com.iveso.doctorum.controller.clinic.response.ClinicDetailResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicIdResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicResponse;
import com.iveso.doctorum.controller.clinic.response.CreateClinicResponse;
import com.iveso.doctorum.exception.clinic.ClinicNotFoundException;
import com.iveso.doctorum.exception.platform.TechnicalException;
import com.iveso.doctorum.mapper.address.CountyMapper;
import com.iveso.doctorum.mapper.clinic.ClinicMapper;
import com.iveso.doctorum.repository.ClinicRepository;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Contact;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.filter.ClinicFilter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class ClinicService {
    private final ClinicRepository repository;
    private final IbgeClient client;

    public List<ClinicResponse> findAll() {
        log.info("Finding all clinics");
        List<Clinic> clinics = repository.findAll();

        log.info("Clinics found. {}", clinics);

        return clinics.stream()
                .map(ClinicMapper.INSTANCE::createResponse)
                .toList();
    }

    public Page<ClinicResponse> filterClinics(FilterClinicsRequest request, PageRequest pageRequest) {
        try {
            log.info("Finding all clinics from page {} and size {} with filters {}", pageRequest.getPageNumber(), pageRequest.getPageSize(), request);

            Page<Clinic> page = repository.findAll(ClinicFilter.newInstance()
                    .withStates(request.states())
                    .withCounties(request.counties())
                    .withExpertises(request.expertises()),
                    pageRequest);

            List<Clinic> clinics = page.toList();

            log.info("Clinics found. {}", clinics);

            return page.map(ClinicMapper.INSTANCE::createResponse);
        } catch(Exception e) {
            log.error(e.getMessage(), e);
            return Page.empty(pageRequest);
        }
    }

    public ClinicIdResponse findById(Integer id) {
        log.info("Searching clinic with id {}.", id);
        Clinic clinic = repository.findById(id).orElseThrow(() -> new ClinicNotFoundException(id));

        log.info("Clinic with id {} founded. {}", id, clinic);
        return ClinicMapper.INSTANCE.clinicResponse(clinic);
    }

    public ClinicDetailResponse findDetailById(Integer id, String username) {
        log.info("{}: Searching clinic detail with id {}.", username, id);
        Clinic clinic = repository.findById(id).orElseThrow(() -> new ClinicNotFoundException(id));

        log.info("{}: Clinic with id {} founded. {}", username, id, clinic);
        return ClinicMapper.INSTANCE.clinicDetailResponse(clinic);
    }

    public CreateClinicResponse create(CreateClinic request) {
        try {
            log.info("Creating clinic from request. {}", request);
            Clinic entity = ClinicMapper.INSTANCE.createEntityFrom(request);

            log.info("Saving clinic entity: {}", entity);
            entity = repository.save(entity);

            CreateClinicResponse response = new CreateClinicResponse(entity.getId());
            log.info("Clinic created. {}", response);
            return response;
        } catch(IllegalArgumentException exception) {
            log.error("An error has occurred on create clinic from request {}. Message: {}", request, exception.getMessage(), exception);
            throw new TechnicalException("Was not possible create clinic", exception);
        }
    }

    @Transactional
    public void update(UpdateClinicRequest request) {
        log.info("Updating clinic with id {}. Request: {}", request.id(), request);

        Clinic clinic = repository.findById(request.id())
                .orElseThrow(() -> new ClinicNotFoundException(request.id()));

        clinic.setDistrict(request.district());
        clinic.setLocation(request.location());
        clinic.setPublicPlace(request.publicPlace());
        clinic.setEmail(request.email());
        clinic.setLatitude(request.latitude());
        clinic.setLongitude(request.longitude());
        clinic.setZipcode(request.zipcode());

        Contact contactUpdated = Optional.ofNullable(clinic.getContact())
                .map(contact -> {
                    contact.setNumber(request.phone());
                    return contact;
                })
                .orElseGet(() -> {
                    Contact contact = new Contact();
                    contact.setNumber(request.phone());
                    contact.setWhatsapp(Boolean.TRUE);
                    return contact;
                });
        clinic.setContact(contactUpdated);

        County county = CountyMapper.INSTANCE.toEntity(request.county());
        clinic.setCounty(county);

        log.info("Clinic with id {} updated.", request.id());
    }
}

package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.visit.request.CreateVisitObservationRequest;
import com.iveso.doctorum.controller.visit.request.UpdateCheckVisitedDoctorRequest;
import com.iveso.doctorum.controller.visit.response.CreateVisitObservationResponse;
import com.iveso.doctorum.controller.visit.response.FindVisitResponse;
import com.iveso.doctorum.exception.platform.PlatformException;
import com.iveso.doctorum.exception.visit.VisitNotFoundException;
import com.iveso.doctorum.exception.visit.VisitObservationAudioException;
import com.iveso.doctorum.mapper.visit.VisitMapper;
import com.iveso.doctorum.repository.ScheduleRepository;
import com.iveso.doctorum.repository.VisitRepository;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.repository.projection.schedule.LocalDateOnly;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class VisitService {
    private final VisitRepository repository;
    private final ScheduleRepository scheduleRepository;

    @Transactional
    public void checkVisit(UpdateCheckVisitedDoctorRequest request) {
        log.info("Checking visit with request {}", request);
        Visit visit = repository.findById(request.id())
                .orElseThrow(() -> new VisitNotFoundException(request.id()));

        visit.setVisited(request.visited());

        log.info("Visit {} checked update", visit.getId());
    }

    public FindVisitResponse findVisitById(int id) {
        try {
            log.info("Finding visit with id {}", id);
            Visit visit = repository.findById(id)
                    .orElseThrow(() -> new VisitNotFoundException(id));

            LocalDateOnly scheduleDate = scheduleRepository.findByVisits(visit);

            log.info("Visit find with date {}. Visit: {}", scheduleDate.date(), visit);
            return VisitMapper.INSTANCE.toFindVisitResponse(visit, scheduleDate.date());
        } catch(PlatformException exception) {
            log.warn("Not possible find visit. Reason: {}", exception.getMessage());
            throw exception;
        }
    }

    @Transactional
    public CreateVisitObservationResponse registerObservation(Integer visitId, String observation, MultipartFile record) {
        try {
            return registerObservation(VisitMapper.INSTANCE.toCreateVisitObservationRequest(
                    visitId, observation, record
            ));
        } catch (IOException exception) {
            log.error("An error has occurred on audio record named {}. Reason: {}", record.getOriginalFilename(), exception.getMessage());
            throw new VisitObservationAudioException(record.getOriginalFilename());
        }
    }

    @Transactional
    public CreateVisitObservationResponse registerObservation(CreateVisitObservationRequest request) {
        try {
            log.info("Registering observation to visit id {}. Request: {}", request.visitId(), request);
            Visit visit = repository.findById(request.visitId())
                    .orElseThrow(() -> new VisitNotFoundException(request.visitId()));

            visit.setObservation(request.observation());
            visit.setVisited(true);

            if(request.record() != null) {
                visit.setRecordName(request.record().title());
                visit.setAudioObservation(request.record().record());
            }

            return new CreateVisitObservationResponse(visit.getId());
        } catch(PlatformException exception) {
            log.warn("Not possible register observation visit. Reason: {}", exception.getMessage());
            throw exception;
        }
    }
}

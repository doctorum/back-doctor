package com.iveso.doctorum.service;

import com.iveso.doctorum.controller.schedule.request.CreateScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.FilterScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.UpdateScheduleRequest;
import com.iveso.doctorum.controller.schedule.response.CreateScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.FindScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.ScheduleResponse;
import com.iveso.doctorum.exception.platform.PlatformException;
import com.iveso.doctorum.exception.schedule.ScheduleNotFoundException;
import com.iveso.doctorum.mapper.schedule.ScheduleMapper;
import com.iveso.doctorum.repository.ScheduleRepository;
import com.iveso.doctorum.repository.VisitRepository;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.repository.filter.ScheduleFilter;
import com.iveso.doctorum.util.LocalDateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleService {
    private final ScheduleRepository repository;
    private final VisitRepository visitRepository;

    public CreateScheduleResponse createSchedule(CreateScheduleRequest request) {
        log.info("Creating schedule from request: {}", request);
        Schedule schedule = ScheduleMapper.INSTANCE.toScheduleEntity(request);

        schedule = repository.save(schedule);

        log.info("Schedule saved. {}", schedule);
        return new CreateScheduleResponse(schedule.getId());
    }

    @Transactional
    public void updateSchedule(UpdateScheduleRequest request) {
        try {
            log.info("Updating schedule from request: {}", request);

            Schedule schedule = repository.findById(request.id())
                    .orElseThrow(() -> new ScheduleNotFoundException(request.id()));

            processRemoved(schedule, request.doctors());
            processAdded(schedule, request.doctors());

            log.info("Schedule updated. {}", schedule);
        } catch(PlatformException exception) {
            log.warn("Not possible update schedule. Reason: {}", exception.getMessage());
            throw exception;
        }
    }

    private void processRemoved(Schedule schedule, List<Integer> doctors) {
        List<Visit> visitsIdRemoved = schedule.getVisits()
                .stream()
                .filter(visit -> !doctors.contains(visit.getDoctor().getId()))
                .toList();

        if(!visitsIdRemoved.isEmpty()) {
            schedule.getVisits().removeIf(visit -> visitsIdRemoved.contains(visit));

            log.info("Removing visits {}", visitsIdRemoved);
            visitRepository.deleteAll(visitsIdRemoved);
        }
    }

    private void processAdded(Schedule schedule, List<Integer> doctors) {
        List<Integer> visitDoctors = schedule.getVisits()
                .stream()
                .map(Visit::getDoctor)
                .map(Doctor::getId)
                .toList();

        List<Visit> visitsAdded = doctors.stream()
                .filter(doctorId -> !visitDoctors.contains(doctorId))
                .map(Doctor::new)
                .map(Visit::new)
                .toList();

        log.info("Adding visits on schedule {}. visits: {}", schedule.getId(), visitsAdded);
        schedule.getVisits().addAll(visitsAdded);
    }

    public List<Integer> listScheduleDays(int month, int year) {
        log.info("Finding days with schedule from month {}/{}", month, year);
        List<Integer> daysWithSchedule = repository.findByDateBetween(LocalDateUtil.firstDate(month, year), LocalDateUtil.lastDate(month, year))
                .stream()
                .map(Schedule::getDate)
                .map(LocalDate::getDayOfMonth)
                .toList();

        log.info("Days with schedule of month {}/{}. {}", month, year, daysWithSchedule);
        return daysWithSchedule;
    }

    @SuppressWarnings("unchecked")
    public ScheduleResponse findSchedules(FilterScheduleRequest request) {
        try {
            log.info("Finding schedules from filter {}.", request);

            Optional<Schedule> response = repository.findOne(ScheduleFilter.newInstance()
                    .withDate(request.date())
                    .withNames(request.names())
                    .withExpertises(request.expertises())
                    .withCounties(request.counties()));

            Schedule schedule = response.orElseThrow(() -> new ScheduleNotFoundException(request));
            log.info("Schedule find. {}", schedule);

            return ScheduleMapper.INSTANCE.toScheduleResponse(schedule);
        } catch(PlatformException exception) {
            log.warn("Not possible find schedule. Reason: {}", exception.getMessage());
            throw exception;
        }
    }

    public FindScheduleResponse findScheduleById(int id) {
        try {
            log.info("Finding schedule with id {}", id);
            Schedule schedule = repository.findById(id)
                    .orElseThrow(() -> new ScheduleNotFoundException(id));

            log.info("Schedule find. {}", schedule);
            return ScheduleMapper.INSTANCE.toFindScheduleResponse(schedule);

        } catch(PlatformException exception) {
            log.warn("Not possible find schedule. Reason: {}", exception.getMessage());
            throw exception;
        }
    }
}

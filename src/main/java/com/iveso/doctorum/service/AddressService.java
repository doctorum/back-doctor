package com.iveso.doctorum.service;

import com.iveso.doctorum.client.IbgeClient;
import com.iveso.doctorum.client.response.StateResponse;
import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.controller.address.response.FillCountiesResponse;
import com.iveso.doctorum.controller.address.response.FillStateResponse;
import com.iveso.doctorum.exception.address.AlreadyExistsCountiesException;
import com.iveso.doctorum.exception.address.AlreadyExistsStateException;
import com.iveso.doctorum.exception.address.AlreadyExistsStatesException;
import com.iveso.doctorum.exception.address.UFNotFoundException;
import com.iveso.doctorum.exception.platform.NotFoundException;
import com.iveso.doctorum.mapper.address.CountyMapper;
import com.iveso.doctorum.mapper.address.StateMapper;
import com.iveso.doctorum.repository.CountyRepository;
import com.iveso.doctorum.repository.StateRepository;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;

@Slf4j
@Service
@AllArgsConstructor
public class AddressService {
    private final IbgeClient ibgeClient;
    private final StateRepository stateRepository;
    private final CountyRepository countyRepository;

    public List<com.iveso.doctorum.controller.address.response.StateResponse> findAll() {
        log.info("Searching all states");
        List<State> states = stateRepository.findAll();

        log.info("States finded. Response: {}", states);
        return states.stream()
                .map(StateMapper.INSTANCE::from)
                .sorted(comparing(com.iveso.doctorum.controller.address.response.StateResponse::uf))
                .toList();
    }

    public List<CountyResponse> findCountiesByState(Integer id) {
        log.info("Searching counties from state id {}.", id);
        List<County> counties = countyRepository.findByStateId(id);

        log.info("Counties finded from state {}. Response: {}", id, counties);
        return counties.stream()
                .map(CountyMapper.INSTANCE::from)
                .toList();
    }

    public FillStateResponse fillStateByUf(String uf) {
        log.info("Verifying if UF {} already exists.", uf);
        stateRepository.findByUf(uf)
                .ifPresent(state -> {
                    throw new AlreadyExistsStateException(state.getUf());
                });

        log.info("Finding UF {}", uf);
        StateResponse stateResponse = ibgeClient.loadStates()
                .stream()
                .filter(state -> state.uf().equalsIgnoreCase(uf))
                .findFirst()
                .orElseThrow(() -> {
                    log.warn("UF {} not found.", uf);
                    return new UFNotFoundException(uf);
                });

        log.info("Converting state to entity. {}", stateResponse);
        State state = StateMapper.INSTANCE.from(stateResponse);

        log.info("Saving state on database. {}", state);
        stateRepository.save(state);

        return new FillStateResponse("State %s save on DB successfully".formatted(uf.toUpperCase()));
    }

    public FillStateResponse fillStates() {
        validateIfAlreadyExecuted();

        log.info("Loading states");
        List<StateResponse> statesResponse = ibgeClient.loadStates();

        log.info("Converting states to entity. {}", statesResponse);
        List<State> states = statesResponse.stream().map(StateMapper.INSTANCE::from).toList();

        log.info("Saving all states on database. {}", states);
        stateRepository.saveAll(states);

        return new FillStateResponse("All states inflate on DB successfully");
    }

    private void validateIfAlreadyExecuted() {
        log.info("Validating if states already register");

        long amount = stateRepository.count();
        if(amount > 0) {
            log.warn("Already exists {} states register on DB.", amount);
            throw new AlreadyExistsStatesException();
        }
    }

    public FillCountiesResponse fillCounties(String uf) {
        log.info("Searching state with UF {}", uf);
        State state = stateRepository.findByUf(uf)
                .orElseThrow(() -> {
                    log.warn("State with uf {} not found.", uf);
                    return new NotFoundException("State with uf %s not found.".formatted(uf));
                });

        validateIfAlreadyExecutedCounties(state);

        log.info("Loading counties");
        List<com.iveso.doctorum.client.response.CountyResponse> countyResponses = ibgeClient.loadCounties(uf);

        log.info("Converting counties to entity. {}", countyResponses);
        List<County> counties = countyResponses.stream()
                .map(countyResponse -> {
                    County county = CountyMapper.INSTANCE.toEntity(countyResponse);
                    county.setState(state);
                    return county;
                }).toList();

        log.info("Saving all counties on database. {}", counties);
        countyRepository.saveAll(counties);
        return new FillCountiesResponse("All counties from UF %s inflate on DB.".formatted(uf));
    }

    private void validateIfAlreadyExecutedCounties(State state) {
        log.info("Validating if counties from state {} already exists", state.getUf());
        long amount = countyRepository.countByState(state);

        if(amount > 0) {
            log.warn("Already exists counties from UF {}", state.getUf());
            throw new AlreadyExistsCountiesException(state.getUf());
        }
    }
}

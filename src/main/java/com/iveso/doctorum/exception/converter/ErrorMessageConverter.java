package com.iveso.doctorum.exception.converter;

import com.iveso.doctorum.exception.response.ErrorMessage;

public class ErrorMessageConverter {
    public ErrorMessage convert(Throwable businessException) {
        return new ErrorMessage(businessException.getMessage());
    }
}

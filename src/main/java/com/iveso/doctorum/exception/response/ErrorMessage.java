package com.iveso.doctorum.exception.response;

public record ErrorMessage(String message) {
}

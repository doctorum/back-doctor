package com.iveso.doctorum.exception.clinic;

import com.iveso.doctorum.exception.platform.NotFoundException;

public class ClinicNotFoundException extends NotFoundException {
    public ClinicNotFoundException(Integer clinicId) {
        super("Clinic with id " + clinicId + " not found");
    }
}

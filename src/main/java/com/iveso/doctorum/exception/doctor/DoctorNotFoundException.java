package com.iveso.doctorum.exception.doctor;

import com.iveso.doctorum.exception.platform.NotFoundException;

public class DoctorNotFoundException extends NotFoundException {
    public DoctorNotFoundException(Integer doctorId) {
        super("Doctor with id " + doctorId + " not found");
    }
}

package com.iveso.doctorum.exception.address;

import com.iveso.doctorum.exception.platform.NotFoundException;

public class UFNotFoundException extends NotFoundException {

    public UFNotFoundException(String uf) {
        super("UF %s not found".formatted(uf.toUpperCase()));
    }
}

package com.iveso.doctorum.exception.address;

import com.iveso.doctorum.exception.platform.BusinessException;

public class AlreadyExistsCountiesException extends BusinessException {

    public AlreadyExistsCountiesException(String uf) {
        super("Already exists counties resgister from UF %s".formatted(uf));
    }
}

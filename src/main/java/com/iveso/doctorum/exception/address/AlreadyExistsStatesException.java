package com.iveso.doctorum.exception.address;

import com.iveso.doctorum.exception.platform.BusinessException;

public class AlreadyExistsStatesException extends BusinessException {

    public AlreadyExistsStatesException() {
        super("Already exists states resgister on DB.");
    }
}

package com.iveso.doctorum.exception.address;

import com.iveso.doctorum.exception.platform.BusinessException;

public class AlreadyExistsStateException extends BusinessException {

    public AlreadyExistsStateException(String uf) {
        super("Already exists states with UF %s resgister on DB.".formatted(uf));
    }
}

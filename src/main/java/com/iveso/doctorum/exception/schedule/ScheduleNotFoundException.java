package com.iveso.doctorum.exception.schedule;

import com.iveso.doctorum.controller.schedule.request.FilterScheduleRequest;
import com.iveso.doctorum.exception.platform.NotFoundException;

public class ScheduleNotFoundException extends NotFoundException {
    public ScheduleNotFoundException(Integer id) {
        super("Schedule with id " + id + " not found");
    }

    public ScheduleNotFoundException(FilterScheduleRequest request) {
        super("Schedule with filters not found. " + request);
    }
}

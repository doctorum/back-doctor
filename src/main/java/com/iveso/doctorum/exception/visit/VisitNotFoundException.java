package com.iveso.doctorum.exception.visit;

import com.iveso.doctorum.exception.platform.NotFoundException;

public class VisitNotFoundException extends NotFoundException {
    public VisitNotFoundException(Integer visitId) {
        super("Visit with id " + visitId + " not found");
    }
}

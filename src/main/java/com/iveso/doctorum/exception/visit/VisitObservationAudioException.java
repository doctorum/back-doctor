package com.iveso.doctorum.exception.visit;

import com.iveso.doctorum.exception.platform.BadGatewayException;

public class VisitObservationAudioException extends BadGatewayException {
    public VisitObservationAudioException(String filename) {
        super("An error has occurred on audio record name " + filename);
    }
}

package com.iveso.doctorum.exception.platform;

public class ForbiddenException extends PlatformException {
    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

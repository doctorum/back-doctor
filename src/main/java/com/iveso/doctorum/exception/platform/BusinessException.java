package com.iveso.doctorum.exception.platform;

public class BusinessException extends PlatformException {
    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

package com.iveso.doctorum.exception.platform;

public class TechnicalException extends PlatformException {
    public TechnicalException(String message) {
        super(message);
    }

    public TechnicalException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

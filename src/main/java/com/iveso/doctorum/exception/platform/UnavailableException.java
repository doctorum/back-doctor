package com.iveso.doctorum.exception.platform;

public class UnavailableException extends PlatformException {
    public UnavailableException(String message) {
        super(message);
    }

    public UnavailableException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

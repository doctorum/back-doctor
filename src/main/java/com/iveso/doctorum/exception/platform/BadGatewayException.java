package com.iveso.doctorum.exception.platform;

public class BadGatewayException extends PlatformException {
    public BadGatewayException(String message) {
        super(message);
    }

    public BadGatewayException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

package com.iveso.doctorum.exception.platform;

public class ConflictException extends PlatformException {
    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

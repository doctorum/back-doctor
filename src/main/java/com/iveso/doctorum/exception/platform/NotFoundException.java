package com.iveso.doctorum.exception.platform;

public class NotFoundException extends PlatformException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

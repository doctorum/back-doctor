package com.iveso.doctorum.exception.platform;

public class BadRequestException extends PlatformException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

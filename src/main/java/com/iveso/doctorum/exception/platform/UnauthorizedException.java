package com.iveso.doctorum.exception.platform;

public class UnauthorizedException extends PlatformException {
    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

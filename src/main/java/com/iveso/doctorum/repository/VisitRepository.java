package com.iveso.doctorum.repository;

import com.iveso.doctorum.repository.entity.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitRepository extends JpaRepository<Visit, Integer> {
}

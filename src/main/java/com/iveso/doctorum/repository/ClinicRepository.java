package com.iveso.doctorum.repository;

import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.filter.ClinicFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicRepository extends JpaRepository<Clinic, Integer>, JpaSpecificationExecutor {
}

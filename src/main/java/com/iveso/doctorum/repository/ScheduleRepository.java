package com.iveso.doctorum.repository;

import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.repository.entity.Visit;
import com.iveso.doctorum.repository.projection.schedule.LocalDateOnly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer>, JpaSpecificationExecutor {
    List<Schedule> findByDateBetween(LocalDate from, LocalDate to);

    LocalDateOnly findByVisits(Visit visit);
}

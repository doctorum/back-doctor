package com.iveso.doctorum.repository.filter;

import com.iveso.doctorum.repository.entity.*;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ScheduleFilter implements Specification<Schedule> {

    private final List<Specification<Schedule>> restrictions;

    private ScheduleFilter() {
        restrictions = new ArrayList<>();
    }

    public static ScheduleFilter newInstance() {
        return new ScheduleFilter();
    }

    public ScheduleFilter withDate(LocalDate date) {
        if(date != null) {
            restrictions.add((schedule, query, builder) -> builder.equal(schedule.get("date"), date));
        }
        return this;
    }

    public ScheduleFilter withNames(List<String> names) {
        if(names != null) {
            restrictions.add((schedule, query, builder) -> {
                ListJoin<Schedule, Visit> visits = schedule.joinList("visits", JoinType.INNER);
                Join<Visit, Doctor> doctor = visits.join("doctor", JoinType.INNER);

                Stream<Predicate> nameStream = names.stream()
                        .map(name -> builder.like(doctor.get("name"), "%" + name + "%"));


                Stream<Predicate> nicknameStream = names.stream()
                        .map(name -> builder.like(doctor.get("nickname"), "%" + name + "%"));

                Predicate[] predicates = Stream.concat(nameStream, nicknameStream)
                        .toArray(Predicate[]::new);

                return builder.or(predicates);
            });
        }
        return this;
    }

    public ScheduleFilter withExpertises(List<String> expertises) {
        if(expertises != null) {
            restrictions.add((schedule, query, builder) -> {
                ListJoin<Schedule, Visit> visits = schedule.joinList("visits", JoinType.INNER);
                Join<Visit, Doctor> doctor = visits.join("doctor", JoinType.INNER);
                return doctor.get("expertise").in(expertises);
            });
        }
        return this;
    }

    public ScheduleFilter withCounties(List<String> counties) {
        if(counties != null) {
            restrictions.add((schedule, query, builder) -> {
                ListJoin<Schedule, Visit> visits = schedule.joinList("visits", JoinType.INNER);
                Join<Visit, Doctor> doctor = visits.join("doctor", JoinType.INNER);
                ListJoin<Doctor, Clinic> clinics = doctor.joinList("clinics", JoinType.INNER);
                Join<Clinic, County> county = clinics.join("county", JoinType.INNER);

                return county.get("name").in(counties);
            });
        }
        return this;
    }

    @Override
    public Predicate toPredicate(Root<Schedule> schedule, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return Specification.allOf(restrictions).toPredicate(schedule, query, builder);
    }
}

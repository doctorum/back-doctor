package com.iveso.doctorum.repository.filter;

import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.Doctor;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class DoctorFilter implements Specification<Doctor> {
    private final List<Specification<Doctor>> restrictions;

    private DoctorFilter() {
        restrictions = new ArrayList<>();
    }

    public static DoctorFilter newInstance() {
        return new DoctorFilter();
    }

    public DoctorFilter withClinicId(Integer clinicId) {
        if(clinicId != null) {
            restrictions.add((doctor, query, builder) -> {
                ListJoin<Doctor, Clinic> clinics = doctor.joinList("clinics", JoinType.INNER);
                return builder.equal(clinics.get("id"), clinicId);
            });
        }
        return this;
    }

    public DoctorFilter withSectors(List<String> sectors) {
        if(sectors != null) {
            restrictions.add((doctor, query, builder) -> doctor.get("sector").in(sectors));
        }
        return this;
    }

    public DoctorFilter withExpertises(List<String> expertises) {
        if(expertises != null) {
            restrictions.add((doctor, query, builder) -> doctor.get("expertise").in(expertises));
        }
        return this;
    }

    public DoctorFilter withCounties(List<String> counties) {
        if(counties != null) {
            restrictions.add((doctor, query, builder) -> {
                ListJoin<Doctor, Clinic> clinics = doctor.joinList("clinics", JoinType.INNER);
                Join<Clinic, County> county = clinics.join("county", JoinType.INNER);
                return county.get("name").in(counties);
            });
        }
        return this;
    }

    @Override
    public Predicate toPredicate(Root<Doctor> doctor, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return Specification.allOf(restrictions).toPredicate(doctor, query, builder);
    }
}

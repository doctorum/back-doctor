package com.iveso.doctorum.repository.filter;

import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.State;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClinicFilter implements Specification<Clinic> {

    private final List<Specification<Clinic>> restrictions;

    private ClinicFilter() {
        restrictions = new ArrayList<>();
    }

    public static ClinicFilter newInstance() {
        return new ClinicFilter();
    }

    public ClinicFilter withStates(List<Integer> states) {
        if(states != null) {
            restrictions.add((clinic, query, builder) -> {
                Join<Clinic, County> county = clinic.join("county", JoinType.INNER);
                Join<County, State> state = county.join("state", JoinType.INNER);
                return state.get("id").in(states);
            });
        }
        return this;
    }

    public ClinicFilter withCounties(List<Integer> counties) {
        if(counties != null) {
            restrictions.add((clinic, query, builder) -> {
                Join<Clinic, County> county = clinic.join("county");
                return county.get("id").in(counties);
            });
        }

        return this;
    }

    public ClinicFilter withExpertises(List<String> expertises) {
        if(expertises != null) {
            restrictions.add((clinic, query, builder) -> {
                ListJoin<Clinic, Collection<Doctor>> doctors = clinic.joinList("doctors", JoinType.INNER);
                return builder.in(doctors.get("expertise")).value(expertises);
            });
        }
        return this;
    }

    @Override
    public Predicate toPredicate(Root<Clinic> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return Specification.allOf(restrictions).toPredicate(root, query, builder);
    }
}

package com.iveso.doctorum.repository.projection.schedule;

import java.time.LocalDate;

public interface DateOnly {
    LocalDate getDate();
}

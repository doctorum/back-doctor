package com.iveso.doctorum.repository.projection.schedule;

import java.time.LocalDate;

public record LocalDateOnly(
        LocalDate date
) {
}

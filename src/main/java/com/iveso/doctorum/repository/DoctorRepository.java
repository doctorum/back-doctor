package com.iveso.doctorum.repository;

import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Integer>, JpaSpecificationExecutor {

    @Query("SELECT d.sector FROM Doctor d GROUP BY d.sector ORDER BY d.sector ASC")
    List<String> findSectors();

    @Query("""
            SELECT clinic.county FROM Doctor doctor
            INNER JOIN doctor.clinics clinic
            GROUP BY clinic.county
            ORDER BY clinic.county.name
            """)
    List<County> findCountiesFromDoctors();
}

package com.iveso.doctorum.repository;

import com.iveso.doctorum.repository.entity.County;
import com.iveso.doctorum.repository.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CountyRepository extends JpaRepository<County, Integer> {
    List<County> findByStateId(Integer stateId);

    long countByState(State state);
}

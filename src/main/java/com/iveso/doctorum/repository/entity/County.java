package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class County implements Serializable {
	@Serial
	private static final long serialVersionUID = 7325226097164483535L;

	@Id
	@Column
	private Integer id;

	@Column(nullable = false)
	private String name;

	@JoinColumn
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private State state;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
}

package com.iveso.doctorum.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class State implements Serializable {
	@Serial
	private static final long serialVersionUID = -5475032595917074132L;

	@Id
	@Column
	private Integer id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String uf;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
}

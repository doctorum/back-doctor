package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

@Getter
@Setter
@Entity
public class Clinic {

    public Clinic() {
        this(null);
    }

    public Clinic(Integer id) {
        this.id = id;
    }

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String publicPlace;

    @Column
    private String district;

    @Column
    private Integer zipcode;

    @Column
    private String location;

    @Column
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "county_id", referencedColumnName = "id")
    private County county;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
    private Contact contact;

    @OneToMany(mappedBy = "clinic", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Attendant> attendants;

    @ManyToMany(mappedBy = "clinics", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Doctor> doctors;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}

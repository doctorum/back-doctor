package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class Doctor extends Person {
	@Serial
	private static final long serialVersionUID = -8182660040948100738L;

	@Column(length = 40)
	private String crm;

	@Column(length = 100)
	private String expertise;

	@Column
	private String sector;

	@Column
	private Integer graduationYear;

	@ManyToMany
	@JoinTable(
			name = "doctor_clinic",
			joinColumns = @JoinColumn(name = "doctorId", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "clinicId", referencedColumnName = "id"))
	private List<Clinic> clinics;

	public Doctor(Integer id) {
		super(id);
	}
}

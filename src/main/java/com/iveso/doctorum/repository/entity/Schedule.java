package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
public class Schedule {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private LocalDate date;

    @OneToMany(cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
    }, targetEntity = Visit.class)
    @JoinColumn(
            name = "schedule_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "FK_SCHEDULE_VISIT")
    )
    private List<Visit> visits;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}

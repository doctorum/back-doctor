package com.iveso.doctorum.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serial;

@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Attendant extends Person {
	@Serial
	private static final long serialVersionUID = 1866962297303867203L;

	@ManyToOne
	@JoinColumn(name = "clinic_id")
	private Clinic clinic;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}

package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Visit {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(
            name = "doctor_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "FK_VISIT_DOCTOR")
    )
    private Doctor doctor;

    @Column
    private String observation;

    @Column
    private boolean visited;

    @Column
    private String recordName;

    @Lob
    @Column(columnDefinition = "LONGBLOB")
    private byte[] audioObservation;

    public Visit(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        ReflectionToStringBuilder.setDefaultStyle(ToStringStyle.JSON_STYLE);
        return ReflectionToStringBuilder.toStringExclude(this, "audioObservation");
    }
}

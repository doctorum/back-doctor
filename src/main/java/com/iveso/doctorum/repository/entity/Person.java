package com.iveso.doctorum.repository.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements Serializable {
	@Serial
	private static final long serialVersionUID = -4760437555669021030L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String nickname;

	@Column(name = "full_name")
	private String name;

	@Column
	private String birth;

	@OneToOne(cascade = CascadeType.ALL)
	private Contact contact;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Email email;

	public Person(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (getClass() != obj.getClass()))
			return false;
		Person other = (Person) obj;
		if (id == null) {
			return other.id == null;
		} else return id.equals(other.id);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}

package com.iveso.doctorum.mapper.clinic;

import com.iveso.doctorum.controller.clinic.request.CreateClinic;
import com.iveso.doctorum.controller.clinic.response.ClinicDetailResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicIdResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClinicMapper {
    ClinicMapper INSTANCE = Mappers.getMapper(ClinicMapper.class);

    @Mapping(target = "publicPlace")
    @Mapping(target = "contact.number", source = "phone")
    Clinic createEntityFrom(CreateClinic request);

    @Mapping(target = "contact", source = "clinic.contact.number")
    @Mapping(target = "county.state", source = "clinic.county.state.uf")
    ClinicResponse createResponse(Clinic clinic);

    @Mapping(target = "name", source = "location")
    @Mapping(target = "phone", source = "contact.number")
    ClinicIdResponse clinicResponse(Clinic clinic);

    @Mapping(target = "name", source = "location")
    @Mapping(target = "county", source = "clinic.county.name")
    @Mapping(target = "uf", source = "clinic.county.state.uf")
    @Mapping(target = "contact", source = "clinic.contact.number")
    ClinicDetailResponse clinicDetailResponse(Clinic clinic);
}

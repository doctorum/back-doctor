package com.iveso.doctorum.mapper.doctor;

import com.iveso.doctorum.controller.doctor.request.CreateDoctor;
import com.iveso.doctorum.controller.doctor.request.UpdateDoctorRequest;
import com.iveso.doctorum.controller.doctor.response.DoctorDetailResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorResponse;
import com.iveso.doctorum.repository.entity.Clinic;
import com.iveso.doctorum.repository.entity.Contact;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Email;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DoctorMapper {
    DoctorMapper INSTANCE = Mappers.getMapper(DoctorMapper.class);

    DoctorResponse createResponse(Doctor doctor);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "expertise", source = "request.expertise.name")
    @Mapping(target = "email.address", source = "request.email")
    @Mapping(target = "contact.number", source = "request.contact")
    @Mapping(target = "contact.whatsapp", constant = "true")
    @Mapping(target = "clinics", source = "request.clinics", qualifiedByName = "clinicsId")
    Doctor createEntityFrom(CreateDoctor request);

    @Mapping(target = "expertise", source = "request.expertise.name")
    @Mapping(target = "email.address", source = "request.email")
    @Mapping(target = "contact.number", source = "request.contact")
    @Mapping(target = "contact.whatsapp", constant = "true")
    @Mapping(target = "clinics", source = "request.clinics", qualifiedByName = "clinicsId")
    Doctor createEntityFrom(UpdateDoctorRequest request);

    @Named("clinicsId")
    default List<Clinic> clinicsId(List<Integer> clinicsId) {
        return clinicsId == null
            ? List.of()
            : clinicsId.stream()
                .map(Clinic::new)
                .toList();
    }

    @Mapping(target = "contact", source = "doctor", qualifiedByName = "contactDetail")
    @Mapping(target = "clinics", source = "doctor.clinics", qualifiedByName = "clinics")
    DoctorDetailResponse doctorDetailResponse(Doctor doctor);

    @Named("contactDetail")
    default DoctorDetailResponse.ContactResponse contactDetailResponse(Doctor doctor) {
        String email = doctor.getEmail() == null ? null : doctor.getEmail().getAddress();
        Long number = doctor.getContact() == null ? null : doctor.getContact().getNumber();

        return new DoctorDetailResponse.ContactResponse(email, number);
    }

    @Named("clinics")
    default List<DoctorDetailResponse.ClinicResponse> clinics(List<Clinic> clinics) {
        return clinics.stream()
                .map(clinic -> new DoctorDetailResponse.ClinicResponse(
                        clinic.getId(),
                        clinic.getLocation(),
                        clinic.getCounty().getState().getUf(),
                        clinic.getCounty().getName()
                ))
                .toList();
    }
}

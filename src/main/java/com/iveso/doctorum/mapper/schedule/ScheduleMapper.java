package com.iveso.doctorum.mapper.schedule;

import com.iveso.doctorum.controller.schedule.request.CreateScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.UpdateScheduleRequest;
import com.iveso.doctorum.controller.schedule.response.FindScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.ScheduleResponse;
import com.iveso.doctorum.repository.entity.Doctor;
import com.iveso.doctorum.repository.entity.Schedule;
import com.iveso.doctorum.repository.entity.Visit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface ScheduleMapper {
    ScheduleMapper INSTANCE = Mappers.getMapper(ScheduleMapper.class);

    @Mapping(target = "visits", source = "request.doctors", qualifiedByName = "toVisits")
    Schedule toScheduleEntity(CreateScheduleRequest request);

    @Mapping(target = "visits", source = "request.doctors", qualifiedByName = "toVisits")
    Schedule toScheduleEntity(UpdateScheduleRequest request);

    @Named("toVisits")
    default List<Visit> toVisits(List<Integer> doctorsId) {
        return doctorsId.stream()
                .map(id -> {
                    Doctor doctor = new Doctor(id);
                    return new Visit(doctor);
                })
                .toList();
    }

    @Mapping(target = "visits", source = "schedule.visits", qualifiedByName = "toVisitsResponse")
    ScheduleResponse toScheduleResponse(Schedule schedule);

    @Named("toVisitsResponse")
    default List<ScheduleResponse.VisitResponse> toVisitsResponse(List<Visit> visits) {
        return visits == null
            ? new ArrayList<>()
            : visits.stream()
                .map(visit -> new ScheduleResponse.VisitResponse(
                        visit.getId(),
                        visit.getDoctor().getNickname(),
                        visit.getDoctor().getName(),
                        visit.getDoctor().getClinics().get(0).getCounty().getName(),
                        visit.getDoctor().getClinics().get(0).getDistrict(),
                        visit.getDoctor().getExpertise(),
                        visit.getDoctor().getClinics().get(0).getLatitude(),
                        visit.getDoctor().getClinics().get(0).getLongitude(),
                        visit.isVisited()
                ))
                .toList();
    }

    @Mapping(target = "doctors", source = "schedule.visits", qualifiedByName = "toDoctorsResponse")
    FindScheduleResponse toFindScheduleResponse(Schedule schedule);

    @Named("toDoctorsResponse")
    default List<FindScheduleResponse.DoctorResponse> toDoctorResponse(List<Visit> visits) {
        return visits.stream()
                .map(visit -> new FindScheduleResponse.DoctorResponse(
                        visit.getDoctor().getId(),
                        visit.getDoctor().getNickname(),
                        visit.getDoctor().getName(),
                        visit.getDoctor().getClinics().get(0).getCounty().getName(),
                        visit.getDoctor().getClinics().get(0).getDistrict(),
                        visit.getDoctor().getExpertise(),
                        visit.isVisited()
                ))
                .toList();
    }
}

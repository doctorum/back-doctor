package com.iveso.doctorum.mapper.address;

import com.iveso.doctorum.controller.address.response.StateResponse;
import com.iveso.doctorum.repository.entity.State;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StateMapper {
    StateMapper INSTANCE = Mappers.getMapper(StateMapper.class);

    State from(com.iveso.doctorum.client.response.StateResponse response);

    StateResponse from(State state);
}

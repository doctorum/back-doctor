package com.iveso.doctorum.mapper.address;

import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.controller.clinic.request.UpdateClinicRequest;
import com.iveso.doctorum.repository.entity.County;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CountyMapper {
    CountyMapper INSTANCE = Mappers.getMapper(CountyMapper.class);

    CountyResponse from(County county);

    County toEntity(com.iveso.doctorum.client.response.CountyResponse response);

    County toEntity(UpdateClinicRequest.County request);
}

package com.iveso.doctorum.mapper.visit;

import com.iveso.doctorum.controller.visit.request.CreateVisitObservationRequest;
import com.iveso.doctorum.controller.visit.response.FindVisitResponse;
import com.iveso.doctorum.repository.entity.Visit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;

@Mapper
public interface VisitMapper {
    VisitMapper INSTANCE = Mappers.getMapper(VisitMapper.class);

    @Mapping(target = "name", source = "visit.doctor.name")
    @Mapping(target = "nickname", source = "visit.doctor.nickname")
    @Mapping(target = "expertise", source = "visit.doctor.expertise")
    @Mapping(target = "district", expression = "java(visit.getDoctor().getClinics().get(0).getDistrict())")
    @Mapping(target = "county", expression = "java(visit.getDoctor().getClinics().get(0).getCounty().getName())")
    FindVisitResponse toFindVisitResponse(Visit visit, LocalDate date);

    @Mapping(target = "record", source = "record", qualifiedByName = "toRecordRequest")
    CreateVisitObservationRequest toCreateVisitObservationRequest(int visitId, String observation, MultipartFile record) throws IOException;

    @Named("toRecordRequest")
    default CreateVisitObservationRequest.RecordRequest toRecordRequest(MultipartFile record) throws IOException {
        return new CreateVisitObservationRequest.RecordRequest(
                record.getOriginalFilename(),
                record.getBytes()
        );
    }
}

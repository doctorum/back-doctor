package com.iveso.doctorum.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LocalDateUtil {

    public static LocalDate firstDate(int month, int year) {
        return LocalDate.of(year, month, 1);
    }

    public static LocalDate lastDate(int month, int year) {
        LocalDate date = LocalDate.of(year, month, 1);
        int lastDay = date.getMonth().length(date.isLeapYear());
        return date.withDayOfMonth(lastDay);
    }
}

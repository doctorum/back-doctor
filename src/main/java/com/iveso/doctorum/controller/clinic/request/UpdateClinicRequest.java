package com.iveso.doctorum.controller.clinic.request;

public record UpdateClinicRequest(
        Integer id,
        String district,
        String location,
        String publicPlace,
        String email,
        Long phone,
        Double latitude,
        Double longitude,
        Integer zipcode,
        County county
) {
    public record County(
            Integer id,
            String name,
            State state
    ) {}

    public record State(
            Integer id,
            String name,
            String uf
    ) {}
}

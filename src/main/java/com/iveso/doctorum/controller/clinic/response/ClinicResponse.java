package com.iveso.doctorum.controller.clinic.response;

public record ClinicResponse(
        Integer id,
        String district,
        String location,
        Long contact,
        County county
) {
    public record County(
            String name,
            String state
    ) {}
}

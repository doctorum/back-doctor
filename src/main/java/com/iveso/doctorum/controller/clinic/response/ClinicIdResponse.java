package com.iveso.doctorum.controller.clinic.response;

public record ClinicIdResponse(
        Integer id,
        String name,
        String publicPlace,
        String district,
        String email,
        Long phone,
        Integer zipcode,
        Double latitude,
        Double longitude,
        County county
) {
    public record County(
            Integer id,
            String name,
            State state
    ) {}

    public record State(
            Integer id,
            String name,
            String uf
    ) {}
}

package com.iveso.doctorum.controller.clinic.request;

import java.util.List;

public record FilterClinicsRequest(
        List<Integer> states,
        List<Integer> counties,
        List<String> expertises
) {
}

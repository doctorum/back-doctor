package com.iveso.doctorum.controller.clinic.response;

public record ClinicDetailResponse(
    Integer id,
    String name,
    String publicPlace,
    String district,
    String county,
    String uf,
    Long contact
) {
}

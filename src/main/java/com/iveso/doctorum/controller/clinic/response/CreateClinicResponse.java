package com.iveso.doctorum.controller.clinic.response;

public record CreateClinicResponse(Integer id) {
}

package com.iveso.doctorum.controller.clinic;

import com.iveso.doctorum.controller.clinic.request.CreateClinic;
import com.iveso.doctorum.controller.clinic.request.FilterClinicsRequest;
import com.iveso.doctorum.controller.clinic.request.UpdateClinicRequest;
import com.iveso.doctorum.controller.clinic.response.ClinicDetailResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicIdResponse;
import com.iveso.doctorum.controller.clinic.response.ClinicResponse;
import com.iveso.doctorum.controller.clinic.response.CreateClinicResponse;
import com.iveso.doctorum.service.ClinicService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@AllArgsConstructor
@RequestMapping("clinics")
@SuppressWarnings("unused")
public class ClinicController {
    private final ClinicService service;

    @GetMapping
    public ResponseEntity<Page<ClinicResponse>> findAll(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            FilterClinicsRequest request
    ) {
        return ResponseEntity.ok(service.filterClinics(request, PageRequest.of(page, size)));
    }

    @GetMapping("{id}")
    public ResponseEntity<ClinicIdResponse> findById(@PathVariable Integer id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @GetMapping("detail/{id}")
    public ResponseEntity<ClinicDetailResponse> findDetailById(@PathVariable Integer id, @AuthenticationPrincipal Jwt jwt) {
        String username = jwt.getClaimAsString("preferred_username");
        return ResponseEntity.ok(service.findDetailById(id, username));
    }

    @PostMapping
    public ResponseEntity<CreateClinicResponse> createClinic(@RequestBody CreateClinic request) {
        CreateClinicResponse response = service.create(request);
        return ResponseEntity
                .created(URI.create("/clinics/" + response.id()))
                .body(response);
    }

    @PutMapping
    public ResponseEntity<Void> updateClinic(@RequestBody UpdateClinicRequest request) {
        service.update(request);
        return ResponseEntity
                .noContent()
                .location(URI.create("/clinics/" + request.id()))
                .build();
    }
}

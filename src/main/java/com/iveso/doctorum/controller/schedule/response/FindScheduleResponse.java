package com.iveso.doctorum.controller.schedule.response;

import java.time.LocalDate;
import java.util.List;

public record FindScheduleResponse(
        Integer id,
        LocalDate date,
        List<DoctorResponse> doctors
) {

    public record DoctorResponse(
            Integer id,
            String nickname,
            String name,
            String county,
            String district,
            String expertise,
            boolean visited
    ) {}
}

package com.iveso.doctorum.controller.schedule.response;

public record CreateScheduleResponse(Integer id) {
}

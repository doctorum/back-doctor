package com.iveso.doctorum.controller.schedule;

import com.iveso.doctorum.controller.schedule.request.CreateScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.FilterScheduleRequest;
import com.iveso.doctorum.controller.schedule.request.UpdateScheduleRequest;
import com.iveso.doctorum.controller.schedule.response.CreateScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.FindScheduleResponse;
import com.iveso.doctorum.controller.schedule.response.ScheduleResponse;
import com.iveso.doctorum.service.ScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
@SuppressWarnings("unused")
@RequestMapping("/schedule")
public class ScheduleController {
    private final ScheduleService service;

    @PostMapping
    public ResponseEntity<Void> createSchedule(@RequestBody CreateScheduleRequest request) {
        CreateScheduleResponse response = service.createSchedule(request);
        return ResponseEntity.created(URI.create("/schedule/" + response.id())).build();
    }

    @PatchMapping
    public ResponseEntity<Void> updateSchedule(@RequestBody UpdateScheduleRequest request) {
        service.updateSchedule(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("days")
    public ResponseEntity<List<Integer>> findSchedulesDays(@RequestParam int month, @RequestParam int year) {
        return ResponseEntity.ok(service.listScheduleDays(month, year));
    }

    @GetMapping
    public ResponseEntity<ScheduleResponse> findSchedule(FilterScheduleRequest request) {
        return ResponseEntity.ok(service.findSchedules(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<FindScheduleResponse> findScheduleById(@PathVariable Integer id) {
        return ResponseEntity.ok(service.findScheduleById(id));
    }
}

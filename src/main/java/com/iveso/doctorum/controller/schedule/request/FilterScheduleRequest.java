package com.iveso.doctorum.controller.schedule.request;

import java.time.LocalDate;
import java.util.List;

public record FilterScheduleRequest(
        LocalDate date,
        List<String> names,
        List<String> expertises,
        List<String> counties
) {
}

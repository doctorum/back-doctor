package com.iveso.doctorum.controller.schedule.request;

import java.util.List;

public record UpdateScheduleRequest(
        Integer id,
        List<Integer> doctors
) {
}

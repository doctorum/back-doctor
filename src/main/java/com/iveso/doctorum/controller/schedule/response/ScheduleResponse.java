package com.iveso.doctorum.controller.schedule.response;

import java.util.List;

public record ScheduleResponse(
        Integer id,
        List<VisitResponse> visits
) {

    public record VisitResponse(
            Integer id,
            String nickname,
            String name,
            String county,
            String district,
            String expertise,
            Double latitude,
            Double longitude,
            boolean visited
    ) {}
}

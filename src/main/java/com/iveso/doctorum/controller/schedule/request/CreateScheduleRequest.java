package com.iveso.doctorum.controller.schedule.request;

import java.time.LocalDate;
import java.util.List;

public record CreateScheduleRequest(
        LocalDate date,
        List<Integer> doctors
) {
}

package com.iveso.doctorum.controller.address.response;

public record FillStateResponse(String message) {
}

package com.iveso.doctorum.controller.address.response;

public record CountyResponse(
        Integer id,
        String name,
        State state
) {
    public record State(
            Integer id,
            String name,
            String uf
    ) {}
}

package com.iveso.doctorum.controller.address;

import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.controller.address.response.FillCountiesResponse;
import com.iveso.doctorum.controller.address.response.FillStateResponse;
import com.iveso.doctorum.controller.address.response.StateResponse;
import com.iveso.doctorum.service.AddressService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("addresses")
public class AddressController {
    private final AddressService service;

    @GetMapping("states")
    public ResponseEntity<List<StateResponse>> listStates() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("counties/state/{id}")
    public ResponseEntity<List<CountyResponse>> listCountiesByState(@PathVariable Integer id) {
        return ResponseEntity.ok(service.findCountiesByState(id));
    }

    @PatchMapping("fill/state/{uf}")
    public ResponseEntity<FillStateResponse> fillStateByUf(@PathVariable String uf) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.fillStateByUf(uf));
    }

    @PatchMapping("fill/states")
    public ResponseEntity<FillStateResponse> fillStates() {
        return ResponseEntity.ok(service.fillStates());
    }

    @PatchMapping("fill/counties/state/{uf}")
    public ResponseEntity<FillCountiesResponse> fillCounties(@PathVariable String uf) {
        return ResponseEntity.ok(service.fillCounties(uf));
    }
}

package com.iveso.doctorum.controller.address.response;

public record StateResponse(
        Integer id,
        Integer ibge,
        String name,
        String uf
) { }

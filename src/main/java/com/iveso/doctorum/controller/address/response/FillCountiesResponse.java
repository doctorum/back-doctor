package com.iveso.doctorum.controller.address.response;

public record FillCountiesResponse(String message) {
}

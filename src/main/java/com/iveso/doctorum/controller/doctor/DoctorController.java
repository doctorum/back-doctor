package com.iveso.doctorum.controller.doctor;

import com.iveso.doctorum.controller.address.response.CountyResponse;
import com.iveso.doctorum.controller.doctor.request.CreateDoctor;
import com.iveso.doctorum.controller.doctor.request.FilterDoctorRequest;
import com.iveso.doctorum.controller.doctor.request.UpdateDoctorRequest;
import com.iveso.doctorum.controller.doctor.response.CreateDoctorResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorDetailResponse;
import com.iveso.doctorum.controller.doctor.response.DoctorResponse;
import com.iveso.doctorum.controller.doctor.response.ExpertiseResponse;
import com.iveso.doctorum.service.DoctorService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("doctors")
public class DoctorController {
    private final DoctorService service;

    @GetMapping
    public ResponseEntity<Page<DoctorResponse>> findAll(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            FilterDoctorRequest request
    ) {
        return ResponseEntity.ok(service.findAll(request, PageRequest.of(page, size)));
    }

    @GetMapping("detail/{id}")
    public ResponseEntity<DoctorDetailResponse> detailDoctor(@PathVariable Integer id) {
        return ResponseEntity.ok(service.detailDoctor(id));
    }

    @GetMapping("clinic/{id}")
    public ResponseEntity<List<DoctorResponse>> findByClinic(@PathVariable Integer id) {
        return ResponseEntity.ok(service.findByClinic(id));
    }

    @GetMapping("expertises")
    public ResponseEntity<List<ExpertiseResponse>> findExpertises() {
        return ResponseEntity.ok(service.findExpertises());
    }

    @GetMapping("sectors")
    public  ResponseEntity<List<String>> findSectors() {
        return ResponseEntity.ok(service.findSectors());
    }

    @GetMapping("counties")
    public ResponseEntity<List<CountyResponse>> findCounties() {
        return ResponseEntity.ok(service.findCounties());
    }

    @PostMapping
    public ResponseEntity<CreateDoctorResponse> createDoctor(@RequestBody CreateDoctor request) {
        CreateDoctorResponse response = service.createDoctor(request);
        return ResponseEntity.created(URI.create("/doctors/" + response.id()))
                .body(response);
    }

    @PutMapping
    public ResponseEntity<Void> updateDoctor(@RequestBody UpdateDoctorRequest request) {
        service.updateDoctor(request);
        return ResponseEntity.noContent().build();
    }
}

package com.iveso.doctorum.controller.doctor.request;

import java.util.List;

public record UpdateDoctorRequest(
        Integer id,
        String name,
        String nickname,
        String birth,
        String email,
        String crm,
        String observation,
        Long contact,
        Expertise expertise,
        Integer graduationYear,
        List<Integer> clinics
) {
    public record Expertise(String name) {}
}

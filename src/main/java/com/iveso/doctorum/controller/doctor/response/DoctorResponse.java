package com.iveso.doctorum.controller.doctor.response;

public record DoctorResponse(
        Integer id,
        String expertise,
        String name,
        String nickname
) {
}

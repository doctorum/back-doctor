package com.iveso.doctorum.controller.doctor.response;

public record CreateDoctorResponse(Integer id) {
}

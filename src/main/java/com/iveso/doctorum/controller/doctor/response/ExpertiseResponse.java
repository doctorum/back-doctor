package com.iveso.doctorum.controller.doctor.response;

public record ExpertiseResponse(String name) {
}

package com.iveso.doctorum.controller.doctor.request;

import java.util.List;

public record FilterDoctorRequest(
        List<String> sectors,
        List<String> expertises,
        List<String> counties
) {
}

package com.iveso.doctorum.controller.doctor.response;

import java.util.List;

public record DoctorDetailResponse(
        Integer id,
        String name,
        String nickname,
        String expertise,
        String birth,
        String crm,
        String sector,
        Integer graduationYear,
        String observation,
        ContactResponse contact,
        List<ClinicResponse> clinics
) {

    public record ContactResponse(
            String email,
            Long number
    ) {}

    public record ClinicResponse(
            Integer id,
            String name,
            String uf,
            String county
    ) {}
}

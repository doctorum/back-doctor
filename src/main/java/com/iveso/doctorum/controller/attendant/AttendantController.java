package com.iveso.doctorum.controller.attendant;

import com.iveso.doctorum.controller.attendant.response.AttendantResponse;
import com.iveso.doctorum.service.AttendantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("attendants")
public class AttendantController {
    private final AttendantService service;

    @GetMapping("clinic/{id}")
    public ResponseEntity<List<AttendantResponse>> findAttendantsByClinic(@PathVariable Integer id) {
        return ResponseEntity.ok(service.findAttendantsByClinic(id));
    }
}

package com.iveso.doctorum.controller.attendant.response;

import java.util.List;

public record AttendantResponse(
        Integer id,
        String nickname,
        List<Contact> contacts,
        List<Email> emails
) {
    public record Contact(
            Integer id,
            boolean isWhatsapp,
            Integer number
    ) {}

    public record Email(
            Integer id,
            String address
    ) {}
}

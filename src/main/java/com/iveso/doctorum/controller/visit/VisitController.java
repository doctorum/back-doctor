package com.iveso.doctorum.controller.visit;

import com.iveso.doctorum.controller.visit.request.CreateVisitObservationRequest;
import com.iveso.doctorum.controller.visit.request.UpdateCheckVisitedDoctorRequest;
import com.iveso.doctorum.controller.visit.response.CreateVisitObservationResponse;
import com.iveso.doctorum.controller.visit.response.FindVisitResponse;
import com.iveso.doctorum.service.VisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("visit")
@SuppressWarnings("unused")
public class VisitController {

    private final VisitService service;

    @PutMapping("check")
    public ResponseEntity<Void> updateVisit(@RequestBody UpdateCheckVisitedDoctorRequest request) {
        service.checkVisit(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}")
    public ResponseEntity<FindVisitResponse> findById(@PathVariable int id) {
        return ResponseEntity.ok(service.findVisitById(id));
    }

    @PostMapping
    public ResponseEntity<CreateVisitObservationResponse> createVisit(@RequestBody CreateVisitObservationRequest request) {
        return ResponseEntity.ok(service.registerObservation(request));
    }

    @PostMapping("file")
    public ResponseEntity<CreateVisitObservationResponse> createVisit(
            @RequestParam Integer visitId,
            @RequestParam String observation,
            @RequestParam("record") MultipartFile file
    ) {
        return ResponseEntity.ok(service.registerObservation(visitId, observation, file));
    }
}

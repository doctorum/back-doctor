package com.iveso.doctorum.controller.visit.response;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDate;

public record FindVisitResponse(
        Integer id,
        LocalDate date,
        String county,
        String district,
        String expertise,
        String name,
        String nickname,
        String observation,
        String recordName,
        byte[] audioObservation
) {
    @Override
    public String toString() {
        ReflectionToStringBuilder.setDefaultStyle(ToStringStyle.JSON_STYLE);
        return ReflectionToStringBuilder.toStringExclude(this, "audioObservation");
    }
}

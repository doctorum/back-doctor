package com.iveso.doctorum.controller.visit.request;

public record UpdateCheckVisitedDoctorRequest(
        Integer id,
        Boolean visited
) {
}

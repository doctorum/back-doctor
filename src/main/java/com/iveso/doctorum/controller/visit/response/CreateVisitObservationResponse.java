package com.iveso.doctorum.controller.visit.response;

public record CreateVisitObservationResponse(int id) {
}

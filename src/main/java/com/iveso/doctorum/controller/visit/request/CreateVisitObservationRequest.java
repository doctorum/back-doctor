package com.iveso.doctorum.controller.visit.request;

public record CreateVisitObservationRequest(
        Integer visitId,
        String observation,
        RecordRequest record
) {
    public record RecordRequest(
            String title,
            byte[] record
    ) {
        @Override
        public String toString() {
            return """
                    {title: %s, size: %d}
                    """.formatted(title, record.length);
        }
    }
}
